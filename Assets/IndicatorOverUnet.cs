﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class IndicatorOverUnet : MonoBehaviour
{
    // do not remove any please add in the last row
    public enum IndicatorType { Unknown, LeftSignal, RightSignal, Handbrake, AntiLockBrake, TractionControl, SeatBelt, EngineWarning, LowFuel, AirBag, TirePressure, ParkingBrake,
        GlowPlug, MasterWarningLight, ForwardCollisionWarning, BrakeWarning, ParkingGear, DriveGear, NeutralGear, ReverseGear}
    public IndicatorType indicatorType;
    // on start up, this is the timer that image turns on, then turn off once timer has reached
    public float TurnOffDelayTimer = 0f;
    private Coroutine startTurnOnKeyTurnOnDelayCor = null;

    private void Start()
    {
        DisableImage();
    }

    private void Update()
    {
    }

    private IEnumerator StartTurnOnKeyTurnOnDelayCor()
    {
        EnableImage();
        yield return new WaitForSeconds(TurnOffDelayTimer);
        DisableImage();
        startTurnOnKeyTurnOnDelayCor = null;
    }

    // call this when turn on key/engine is detected
    public void StartTurnOnKeyTurnOnDelay()
    {
        if (TurnOffDelayTimer > 0f)
        {
            startTurnOnKeyTurnOnDelayCor = StartCoroutine(StartTurnOnKeyTurnOnDelayCor());
        }
    }

    public void StartTurnOffKeyTurnOnDelay()
    {
        if (startTurnOnKeyTurnOnDelayCor != null)
        {
            StopCoroutine(startTurnOnKeyTurnOnDelayCor);
            DisableImage();
            startTurnOnKeyTurnOnDelayCor = null;
        }
    }

    public void EnableImage()
    {
        if (this.GetComponent<Image>() != null)
        {
            this.GetComponent<Image>().enabled = true;
        }
        else if (this.GetComponent<RawImage>() != null)
        {
            this.GetComponent<RawImage>().enabled = true;
        }
        else if (this.GetComponent<TextMeshProUGUI>() != null)
        {
            this.GetComponent<TextMeshProUGUI>().enabled = true;
        }
    }

    public void DisableImage()
    {
        if (this.GetComponent<Image>() != null)
        {
            this.GetComponent<Image>().enabled = false;
        }
        else if (this.GetComponent<RawImage>() != null)
        {
            this.GetComponent<RawImage>().enabled = false;
        }
        else if (this.GetComponent<TextMeshProUGUI>() != null)
        {
            this.GetComponent<TextMeshProUGUI>().enabled = false;
        }
    }
}
