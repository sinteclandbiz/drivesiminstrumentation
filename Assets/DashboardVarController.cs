﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DriveSim;
using System.Linq;

public class DashboardVarController : MonoBehaviour
{
    DashboardVarObj dashboardBoardUnetVarObj = new DashboardVarObj();

    // all indicators gameobjects in the scene
    public List<IndicatorOverUnet> AllIndicatorList = new List<IndicatorOverUnet>();
    
    public bool AirBagTrigger = false;
    public bool AntiLockBrake = false;
    public bool BrakeWarning = false;
    public bool DriveGear = false;
    public bool EngineWarning = false;
    public bool EngineStart = false;
    public bool ForwardCollisionWarning = false;
    public bool GlowPlug = false;
    public bool Handbrake = false;
    public bool HazardLightTrigger = false;
    public bool LeftSignal = false;
    public bool LowFuel = false;
    public bool MasterWarningLight = false;
    public bool NeutralGear = false;
    public bool ParkingBrake = false;
    public bool ParkingGear = false;
    public bool RightSignal = false;
    public bool ReverseGear = false;
    public bool SeatBelt = false;
    public bool TractionControl = false;
    public bool TirePressure = false;

    public bool FlickeringHazardLightCorIsRunning = false;
    public bool FlickeringLeftSignalCorIsRunningInTheBackground = false;
    public bool FlickeringRightSignalCorIsRunningInTheBackground = false;
    private Coroutine FlickeringHazardLightCor, FlickeringLeftSignalCor, FlickeringRightSignalCor;
    private NeedleSpeedometerUnet needleSpeedometerUnet;
    private NeedleRPMUnet needleRPMUnet;
    public static DashboardVarController instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        AllIndicatorList = (List<IndicatorOverUnet>)FindObjectsOfType<IndicatorOverUnet>().ToList();
        needleSpeedometerUnet = FindObjectOfType<NeedleSpeedometerUnet>();
        needleRPMUnet = FindObjectOfType<NeedleRPMUnet>();

        // disable speedo and rpm meter on start
        //needleSpeedometerUnet.enabled = false;
        //needleRPMUnet.enabled = false;
    }

    public IndicatorOverUnet GetIndicatorOfType(IndicatorOverUnet.IndicatorType indicatorType)
    {
        foreach (IndicatorOverUnet indicatorOverUnet in AllIndicatorList)
        {
            if (indicatorOverUnet.indicatorType == indicatorType)
            {
                return indicatorOverUnet;
            }
        }

        return null;
    }

    public List<IndicatorOverUnet> GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType indicatorType)
    {
        List<IndicatorOverUnet> IndicatorsToReturn = new List<IndicatorOverUnet>();

        foreach (IndicatorOverUnet indicatorOverUnet in AllIndicatorList)
        {
            if (indicatorOverUnet.indicatorType == indicatorType)
            {
                IndicatorsToReturn.Add(indicatorOverUnet);
            }
        }

        return IndicatorsToReturn;
    }

    // Update is called once per frame
    void Update()
    {
        if (AirBagTrigger != NetworkClientController.instance.uNetVarObj.dashboardVarObj.AirBagOn)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.AirBagOn == true)
            {
                Debug.Log("Air Bag TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.AirBag).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.AirBag))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("Air Bag TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.AirBag).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.AirBag))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (AntiLockBrake != NetworkClientController.instance.uNetVarObj.dashboardVarObj.AntiLockBrake)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.AntiLockBrake == true)
            {
                Debug.Log("AntiLockBrake TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.AntiLockBrake).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.AntiLockBrake))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("AntiLockBrake TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.AntiLockBrake).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.AntiLockBrake))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (BrakeWarning != NetworkClientController.instance.uNetVarObj.dashboardVarObj.BrakeWarning)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.BrakeWarning == true)
            {
                Debug.Log("BrakeWarning TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.BrakeWarning).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.BrakeWarning))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("BrakeWarning TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.BrakeWarning).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.BrakeWarning))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (DriveGear != NetworkClientController.instance.uNetVarObj.dashboardVarObj.DriveGear)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.DriveGear == true)
            {
                Debug.Log("DriveGear TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.DriveGear).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.DriveGear))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("DriveGear TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.DriveGear).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.DriveGear))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (EngineStart != NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineStart)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineStart == true)
            {
                Debug.Log("EngineStart TRIGGERED");

                // turn on all indicators // on start warm up indicators
                foreach (IndicatorOverUnet indicatorOverUnet in AllIndicatorList)
                {
                    indicatorOverUnet.StartTurnOnKeyTurnOnDelay();
                }
            }
            else
            {
                Debug.Log("EngineStart TRIGGERED OFF");

                // turn off all indicators // on start warm up indicators
                foreach (IndicatorOverUnet indicatorOverUnet in AllIndicatorList)
                {
                    indicatorOverUnet.StartTurnOffKeyTurnOnDelay();
                }
            }
        }

        if (EngineWarning != NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineWarning)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineWarning == true)
            {
                Debug.Log("EngineWarning TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.EngineWarning).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.EngineWarning))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("EngineWarning TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.EngineWarning).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.EngineWarning))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (ForwardCollisionWarning != NetworkClientController.instance.uNetVarObj.dashboardVarObj.ForwardCollisionWarning)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.ForwardCollisionWarning == true)
            {
                Debug.Log("ForwardCollisionWarning TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ForwardCollisionWarning).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ForwardCollisionWarning))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("ForwardCollisionWarning TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ForwardCollisionWarning).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ForwardCollisionWarning))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (GlowPlug != NetworkClientController.instance.uNetVarObj.dashboardVarObj.GlowPlug)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.GlowPlug == true)
            {
                Debug.Log("GlowPlug TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.GlowPlug).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.GlowPlug))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("GlowPlug TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.GlowPlug).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.GlowPlug))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (Handbrake != NetworkClientController.instance.uNetVarObj.dashboardVarObj.Handbrake)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.Handbrake == true)
            {
                Debug.Log("Handbrake TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.Handbrake).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.Handbrake))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("Handbrake TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.Handbrake).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.Handbrake))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        // for detecting triggering of variables
        if (HazardLightTrigger != NetworkClientController.instance.uNetVarObj.dashboardVarObj.HazardLightOn)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.HazardLightOn == true)
            {
                //WorldState.Instance.OwnShip.GetComponent<Ownship>().HazardLightState.Value = true;
                //WorldState.Instance.OwnShip.GetComponent<Ownship>().vehicleModel.IsHazardLightsOn = true;
                Debug.Log("HAZARD LIGHT TRIGGERED");

                if (FlickeringLeftSignalCor != null)
                {
                    StopCoroutine(FlickeringLeftSignalCor);
                    FlickeringLeftSignalCorIsRunningInTheBackground = true;
                }
                if (FlickeringRightSignalCor != null)
                {
                    StopCoroutine(FlickeringRightSignalCor);
                    FlickeringRightSignalCorIsRunningInTheBackground = true;
                }

                // start FlickeringHazardLight coroutine
                FlickeringHazardLightCor = StartCoroutine(FlickeringHazardLight(GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LeftSignal),
                    GetIndicatorOfType(IndicatorOverUnet.IndicatorType.RightSignal)));
                FlickeringHazardLightCorIsRunning = true;
            }
            else
            {
                //WorldState.Instance.OwnShip.GetComponent<Ownship>().HazardLightState.Value = false;
                //WorldState.Instance.OwnShip.GetComponent<Ownship>().vehicleModel.IsHazardLightsOn = false;
                Debug.Log("HAZARD LIGHT TRIGGERED OFF");

                // stop FlickeringHazardLight coroutine
                if (FlickeringHazardLightCor != null)
                {
                    StopCoroutine(FlickeringHazardLightCor);
                    FlickeringHazardLightCorIsRunning = false;

                    // turn off all indicators
                    GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LeftSignal).DisableImage();
                    GetIndicatorOfType(IndicatorOverUnet.IndicatorType.RightSignal).DisableImage();
                }
                //StopAllCoroutines();
                if (FlickeringLeftSignalCorIsRunningInTheBackground == true)
                {
                    // start FlickeringLeftSignalLight coroutine
                    FlickeringLeftSignalCor = StartCoroutine(FlickeringLeftSignalLight(GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LeftSignal)));

                    FlickeringLeftSignalCorIsRunningInTheBackground = false;
                }
                else if (FlickeringRightSignalCorIsRunningInTheBackground == true)
                {
                    // start FlickeringRightSignalLight coroutine
                    FlickeringRightSignalCor = StartCoroutine(FlickeringRightSignalLight(GetIndicatorOfType(IndicatorOverUnet.IndicatorType.RightSignal)));

                    FlickeringRightSignalCorIsRunningInTheBackground = false;
                }
            }
        }

        if (LeftSignal != NetworkClientController.instance.uNetVarObj.dashboardVarObj.LeftSignal)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.LeftSignal == true)
            {
                Debug.Log("LeftSignal TRIGGERED");

                // if flickering hazard light corutine is running, do not start flicering left signal cor
                if (FlickeringHazardLightCorIsRunning == true)
                {
                    FlickeringLeftSignalCorIsRunningInTheBackground = true;
                }
                // if flickering hazard light corutine is not running, start flicerikng left signal cor
                else
                {
                    // start FlickeringLeftSignalLight coroutine
                    FlickeringLeftSignalCor = StartCoroutine(FlickeringLeftSignalLight(GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LeftSignal)));

                    FlickeringLeftSignalCorIsRunningInTheBackground = false;
                }
            }
            else
            {
                Debug.Log("LeftSignal TRIGGERED OFF");
                // stop FlickeringLeftSignalLight coroutine
                if (FlickeringLeftSignalCor != null)
                {
                    StopCoroutine(FlickeringLeftSignalCor);
                    FlickeringLeftSignalCor = null;
                    if (FlickeringHazardLightCorIsRunning == false)
                    {
                        // turn off all indicators
                        GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LeftSignal).DisableImage();
                    }
                }
                //StopAllCoroutines();
                FlickeringLeftSignalCorIsRunningInTheBackground = false;

            }
        }

        if (LowFuel != NetworkClientController.instance.uNetVarObj.dashboardVarObj.LowFuel)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.LowFuel == true)
            {
                Debug.Log("LowFuel TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LowFuel).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.LowFuel))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("LowFuel TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.LowFuel).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.LowFuel))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (MasterWarningLight != NetworkClientController.instance.uNetVarObj.dashboardVarObj.MasterWarningLight)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.MasterWarningLight == true)
            {
                Debug.Log("MasterWarningLight TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.MasterWarningLight).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.MasterWarningLight))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("MasterWarningLight TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.MasterWarningLight).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.MasterWarningLight))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (NeutralGear != NetworkClientController.instance.uNetVarObj.dashboardVarObj.NeutralGear)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.NeutralGear == true)
            {
                Debug.Log("NeutralGear TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.NeutralGear).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.NeutralGear))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("NeutralGear TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.NeutralGear).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.NeutralGear))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (ParkingBrake != NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingBrake)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingBrake == true)
            {
                Debug.Log("ParkingBrake TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingBrake).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingBrake))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("ParkingBrake TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingBrake).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingBrake))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (ParkingGear != NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingGear)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingGear == true)
            {
                Debug.Log("ParkingGear TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingGear).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingGear))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("ParkingGear TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingGear).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ParkingGear))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (RightSignal != NetworkClientController.instance.uNetVarObj.dashboardVarObj.RightSignal)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.RightSignal == true)
            {
                Debug.Log("RightSignal TRIGGERED");
                // if flickering hazard light corutine is running, do not start flicering right signal cor
                if (FlickeringHazardLightCorIsRunning == true)
                {
                    FlickeringRightSignalCorIsRunningInTheBackground = true;
                }
                // if flickering hazard light corutine is not running, start flicerikng right signal cor
                else
                {
                    // start FlickeringRightSignalLight coroutine
                    FlickeringRightSignalCor = StartCoroutine(FlickeringRightSignalLight(GetIndicatorOfType(IndicatorOverUnet.IndicatorType.RightSignal)));

                    FlickeringRightSignalCorIsRunningInTheBackground = false;
                }
            }
            else
            {
                Debug.Log("RightSignal TRIGGERED OFF");
                // stop FlickeringRightSignalLight coroutine
                if (FlickeringRightSignalCor != null)
                {
                    StopCoroutine(FlickeringRightSignalCor);
                    FlickeringRightSignalCor = null;
                    if (FlickeringHazardLightCorIsRunning == false)
                    {
                        // turn off all indicators
                        GetIndicatorOfType(IndicatorOverUnet.IndicatorType.RightSignal).DisableImage();
                    }
                }
                FlickeringRightSignalCorIsRunningInTheBackground = false;
            }
        }

        if (ReverseGear != NetworkClientController.instance.uNetVarObj.dashboardVarObj.ReverseGear)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.ReverseGear == true)
            {
                Debug.Log("ReverseGear TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ReverseGear).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ReverseGear))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("ReverseGear TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.ReverseGear).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.ReverseGear))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (SeatBelt != NetworkClientController.instance.uNetVarObj.dashboardVarObj.SeatBelt)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.SeatBelt == true)
            {
                Debug.Log("SeatBelt TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.SeatBelt).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.SeatBelt))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("SeatBelt TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.SeatBelt).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.SeatBelt))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (TractionControl != NetworkClientController.instance.uNetVarObj.dashboardVarObj.TractionControl)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.TractionControl == true)
            {
                Debug.Log("TractionControl TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.TractionControl).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.TractionControl))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("TractionControl TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.TractionControl).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.TractionControl))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }

        if (TirePressure != NetworkClientController.instance.uNetVarObj.dashboardVarObj.TirePressure)
        {
            if (NetworkClientController.instance.uNetVarObj.dashboardVarObj.TirePressure == true)
            {
                Debug.Log("TirePressure TRIGGERED");

                // turn on indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.TirePressure).EnableImage();
                // turn on all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.TirePressure))
                {
                    indicatorOverUNet.EnableImage();
                }
            }
            else
            {
                Debug.Log("TirePressure TRIGGERED OFF");

                // turn off indicator
                //GetIndicatorOfType(IndicatorOverUnet.IndicatorType.TirePressure).DisableImage();
                // turn off all indicator
                foreach (IndicatorOverUnet indicatorOverUNet in GetAllIndicatorOfType(IndicatorOverUnet.IndicatorType.TirePressure))
                {
                    indicatorOverUNet.DisableImage();
                }
            }
        }
    }

    private void LateUpdate()
    {
        // for detecting triggering of variables   
        AirBagTrigger = NetworkClientController.instance.uNetVarObj.dashboardVarObj.AirBagOn;
        AntiLockBrake = NetworkClientController.instance.uNetVarObj.dashboardVarObj.AntiLockBrake;
        BrakeWarning = NetworkClientController.instance.uNetVarObj.dashboardVarObj.BrakeWarning;
        DriveGear = NetworkClientController.instance.uNetVarObj.dashboardVarObj.DriveGear;
        EngineWarning = NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineWarning;
        EngineStart = NetworkClientController.instance.uNetVarObj.dashboardVarObj.EngineStart;
        ForwardCollisionWarning = NetworkClientController.instance.uNetVarObj.dashboardVarObj.ForwardCollisionWarning;
        GlowPlug = NetworkClientController.instance.uNetVarObj.dashboardVarObj.GlowPlug;
        Handbrake = NetworkClientController.instance.uNetVarObj.dashboardVarObj.Handbrake;
        HazardLightTrigger = NetworkClientController.instance.uNetVarObj.dashboardVarObj.HazardLightOn;
        LeftSignal = NetworkClientController.instance.uNetVarObj.dashboardVarObj.LeftSignal;
        LowFuel = NetworkClientController.instance.uNetVarObj.dashboardVarObj.LowFuel;
        MasterWarningLight = NetworkClientController.instance.uNetVarObj.dashboardVarObj.MasterWarningLight;
        NeutralGear = NetworkClientController.instance.uNetVarObj.dashboardVarObj.NeutralGear;
        ParkingBrake = NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingBrake;
        ParkingGear = NetworkClientController.instance.uNetVarObj.dashboardVarObj.ParkingGear;
        RightSignal = NetworkClientController.instance.uNetVarObj.dashboardVarObj.RightSignal;
        ReverseGear = NetworkClientController.instance.uNetVarObj.dashboardVarObj.ReverseGear;
        SeatBelt = NetworkClientController.instance.uNetVarObj.dashboardVarObj.SeatBelt;
        TractionControl = NetworkClientController.instance.uNetVarObj.dashboardVarObj.TractionControl;
        TirePressure = NetworkClientController.instance.uNetVarObj.dashboardVarObj.TirePressure;
    }

    public IEnumerator FlickeringLeftSignalLight(IndicatorOverUnet leftSignalIndicator)
    {
        float timer;
        leftSignalIndicator.EnableImage();
        //rightSignalIndicator.EnableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        leftSignalIndicator.DisableImage();
        //rightSignalIndicator.DisableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        FlickeringLeftSignalCor = StartCoroutine(FlickeringLeftSignalLight(leftSignalIndicator));
    }

    public IEnumerator FlickeringRightSignalLight(IndicatorOverUnet rightSignalIndicator)
    {
        float timer;
        rightSignalIndicator.EnableImage();
        //rightSignalIndicator.EnableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        rightSignalIndicator.DisableImage();
        //rightSignalIndicator.DisableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        FlickeringRightSignalCor = StartCoroutine(FlickeringRightSignalLight(rightSignalIndicator));
    }

    public IEnumerator FlickeringHazardLight(IndicatorOverUnet leftSignalIndicator, IndicatorOverUnet rightSignalIndicator)
    {
        float timer;
        leftSignalIndicator.EnableImage();
        rightSignalIndicator.EnableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        leftSignalIndicator.DisableImage();
        rightSignalIndicator.DisableImage();
        timer = 1;
        yield return new WaitForSeconds(timer);
        FlickeringHazardLightCor = StartCoroutine(FlickeringHazardLight(leftSignalIndicator, rightSignalIndicator));
    }
}
