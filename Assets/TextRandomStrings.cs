﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextRandomStrings : MonoBehaviour
{
    private TextMeshProUGUI textMeshPro;
    public int RandomStringInterval = 2;
    public List<string> RandomStringsToDisplay = new List<string>();
    // Start is called before the first frame update
    void Start()
    {
        textMeshPro = this.GetComponent<TextMeshProUGUI>();

        // set a random string
        textMeshPro.text = RandomStringsToDisplay[Random.Range(0, RandomStringsToDisplay.Count)];

        // start a forever loop
        StartCoroutine(SetRandomStrings());
    }

    public IEnumerator SetRandomStrings()
    {
        while (true)
        {
            yield return new WaitForSeconds(RandomStringInterval);
            textMeshPro.text = RandomStringsToDisplay[Random.Range(0, RandomStringsToDisplay.Count)];
        }
    }
}
