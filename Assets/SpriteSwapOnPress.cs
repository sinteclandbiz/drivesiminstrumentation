﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpriteSwapOnPress : MonoBehaviour
{
    public bool buttonSelected = false;
    public List<Sprite> SpriteListToChange = new List<Sprite>();

    public int SpriteToChangeIndex = 0;
    //public UnityEngine.UI.Button thisButton;
    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(() => ChangeSprite());
        //thisButton = this.GetComponent<UnityEngine.UI.Button>();
        //thisButton.onClick.AddListener(() => ChangeSprite());
    }

    public bool GetButtonSelectedOrNot()
    {
        return buttonSelected;
    }

    public void ChangeSprite()
    {
        SpriteToChangeIndex++;
        if (SpriteToChangeIndex < SpriteListToChange.Count && SpriteToChangeIndex != 0)
        {
            buttonSelected = true;
            this.GetComponent<Image>().sprite = SpriteListToChange[SpriteToChangeIndex];
        }
        else
        {
            buttonSelected = false;

            SpriteToChangeIndex = 0;
            this.GetComponent<Image>().sprite = SpriteListToChange[SpriteToChangeIndex];            
        }
    }

    public void ChangeSpriteToSelected()
    {
        buttonSelected = true;

        SpriteToChangeIndex = 1;
        this.GetComponent<Image>().sprite = SpriteListToChange[SpriteToChangeIndex];
    }

    public void ChangeSpriteToUnSelected()
    {
        buttonSelected = false;

        SpriteToChangeIndex = 0;
        this.GetComponent<Image>().sprite = SpriteListToChange[0];
    }
}
