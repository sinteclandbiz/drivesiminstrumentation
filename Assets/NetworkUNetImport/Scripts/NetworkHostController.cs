﻿// this script is stored on DriveSimUnity (The Host)
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkHostController : MonoBehaviour
{
    public GameObject HostGameObject;
    public InputField sendThisMessageToClientsInputField;
    public List<GameObject> AllClientGameObjects = new List<GameObject>();

    public static NetworkHostController instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    private void Update()
    {
        //// this will type out the string messages to each player
        //if (Input.GetKeyDown(KeyCode.G))
        //{
        //    DebugTextController.instance.AddDebugText("START CLIENT MESSAGES: ");
        //    foreach (GameObject ClientGO in AllClientGameObjects)
        //    {
        //        //Debug.Log ("MY IP ADDRESS IS : " + ClientGO.GetComponent<Networkide)
        //        // recieved messages from instrumentation
        //        DebugTextController.instance.AddDebugText(ClientGO.GetComponent<Player_SendMessageToServer>().syncString);
        //    }

        //    // recieved message from host
        //    DebugTextController.instance.AddDebugText(HostGameObject.GetComponent<Player_SendMessageToServer>().syncServerToClientString);
        //}
    }

    public void GetClientMessageButtonPressed()
    {
        DebugTextController.instance.AddDebugText("START CLIENT MESSAGES: ");
        foreach (GameObject ClientGO in AllClientGameObjects)
        {
            //Debug.Log ("MY IP ADDRESS IS : " + ClientGO.GetComponent<Networkide)
            // recieved messages from instrumentation
            DebugTextController.instance.AddDebugText(ClientGO.GetComponent<Player_SendMessageToServer>().syncClientToServerString);
        }

        //// recieved message from host
        //DebugTextController.instance.AddDebugText(HostGameObject.GetComponent<Player_SendMessageToServer>().syncServerToClientString);
    }

    public void RegisterHostGO(GameObject HostGOToAdd)
    {
        if (HostGameObject == null)
        {
            HostGameObject = HostGOToAdd;
        }
    }

    public bool CheckIfThereIsHostGO()
    {
        if (HostGameObject != null)
        {
            return true;
        }
        return false;
    }

    public void AddClientGameObject(GameObject ClientGOToAdd)
    {
        AllClientGameObjects.Add(ClientGOToAdd);
    }

    public void RemoveClientGameObject(GameObject ClientGOToRemove)
    {
        AllClientGameObjects.Remove(ClientGOToRemove);
    }

    public List<GameObject> GetAllClientsGOs()
    {
        return AllClientGameObjects;
    }
}
