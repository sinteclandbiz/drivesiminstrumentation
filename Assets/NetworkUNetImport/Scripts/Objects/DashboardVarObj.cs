﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DashboardVarObj
{
    public float speed;
    public int rpm;
    public bool HazardLightOn = false;
    public bool AirBagOn = false;
    public bool AntiLockBrake = false;
    public bool BrakeWarning = false;
    public bool DriveGear = false;
    public bool EngineWarning = false;
    public bool EngineStart = false;
    public bool ForwardCollisionWarning = false;
    public bool GlowPlug = false;
    public bool Handbrake = false;
    public bool HeadlightLowBeam = false;
    public bool HeadlightHighBeam = false;
    public bool LeftSignal = false;
    public bool LowFuel = false;
    public bool MasterWarningLight = false;
    public bool NeutralGear = false;
    public bool ParkingBrake = false;
    public bool ParkingGear = false;
    public bool RightSignal = false;
    public bool ReverseGear = false;
    public bool SeatBelt = false;
    public bool TractionControl = false;
    public bool TirePressure = false;
}