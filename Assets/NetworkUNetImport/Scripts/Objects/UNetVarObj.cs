﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UNetVarObj
{
    // dashboard values
    public DashboardVarObj dashboardVarObj;
    // switchboard values
    public SwitchboardVarObj switchboardVarObj;
}