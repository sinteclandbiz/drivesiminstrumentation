﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class SwitchboardVarObj
{
    public bool HazardLightOn = false;
    public bool SirenLightOn = false;

    public bool SirenSoundWailOn = false;
    public bool SirenSoundYelpOn = false;
    public bool SirenSoundHiLowOn = false;
    public bool SirenSoundPiercerOn = false;

    public bool SirenSoundWailRumbleOn = false;
    public bool SirenSoundYelpRumbleOn = false;
    public bool SirenSoundHiLowRumbleOn = false;
    public bool SirenSoundPiercerRumbleOn = false;
}