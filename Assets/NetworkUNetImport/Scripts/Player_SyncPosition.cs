﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Player_SyncPosition : NetworkBehaviour
{
    public float moveSpeed = 1f;

    [SyncVar]
    private Vector3 syncPos;

    [SerializeField] Transform myTransform;
    [SerializeField] float lerpRate = 15f;

    // Update is called once per frame
    void FixedUpdate()
    {
        // client send position of this to server
        TransmitPosition();

        // server will determine if this is client gameobject, if it is then use syncPos to update positions across all clients
        LerpPosition();
    }

    private void Update()
    {
        // controls the movement by keyboard if this gameobject is ours
        if (isLocalPlayer)
        {
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(horizontal, 0f, vertical);
            transform.position += movement * Time.deltaTime * moveSpeed;
        }
    }

    void LerpPosition()
    {
        // supposed to be server synching the position across all clients?
        if (!isLocalPlayer)
        {
            myTransform.position = Vector3.Lerp(myTransform.position, syncPos, Time.deltaTime * lerpRate);
        }
    }

    [Command]
    void CmdProvidePositionToServer(Vector3 pos)
    {
        syncPos = pos;
    }

    [ClientCallback]
    void TransmitPosition()
    {
        if (isLocalPlayer)
        {
            CmdProvidePositionToServer(myTransform.position);
        }
    }
}
