﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugTextController : MonoBehaviour
{
    public Text DebugText;
    public static DebugTextController instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void AddDebugText(string contents)
    {
        DebugText.text += "\n" + contents;
    }
}
