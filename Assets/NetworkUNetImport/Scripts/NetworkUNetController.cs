﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class NetworkUNetController : NetworkManager
{
    public InputField ipAddressInputField;

    public static NetworkUNetController instance { get; private set; }

    private void Start()
    {
        instance = this;

        //networkAddress = ipAddressInputField.text;
        // take the drive sim unity ip address from config file
        networkAddress = MiniLoader.Instance.ioCtrlAddr;
        Debug.Log("Starting Client : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);
        StartClient();
    }

    public void StartHostButtonPressed()
    {
        networkAddress = ipAddressInputField.text;
        Debug.Log("Starting Host : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);
        StartHost();
    }

    public void StartClientButtonPressed()
    {
        networkAddress = ipAddressInputField.text;
        Debug.Log("Starting Client : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);

        StartClient();
    }

    public void StopHostButtonPressed()
    {
        Debug.Log("Stopping Host : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);

        StopHost();
    }

    public void StopClientButtonPressed()
    {
        Debug.Log("Stopping Client : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);

        StopClient();
    }
    // Server callbacks
    public override void OnServerConnect(NetworkConnection conn)
    {
        Debug.Log("A client connected to the server: " + conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        NetworkServer.DestroyPlayersForConnection(conn);
        if (conn.lastError != NetworkError.Ok)
        {
            if (LogFilter.logError) { Debug.LogError("ServerDisconnected due to error: " + conn.lastError); }
        }
        Debug.Log("A client disconnected from the server: " + conn);
    }
    public override void OnServerReady(NetworkConnection conn)
    {
        NetworkServer.SetClientReady(conn);
        Debug.Log("Client is set to the ready state (ready to receive state updates): " + conn);
    }
    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        var player = (GameObject)GameObject.Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
        Debug.Log("Client has requested to get his player added to the game");
    }

    public override void OnServerRemovePlayer(NetworkConnection conn, PlayerController player)
    {
        if (player.gameObject != null)
            NetworkServer.Destroy(player.gameObject);
    }

    public override void OnServerError(NetworkConnection conn, int errorCode)
    {
        Debug.Log("Server network error occurred: " + (NetworkError)errorCode);
    }

    public override void OnStartHost()
    {
        Debug.Log("Host has started");
    }

    public override void OnStartServer()
    {
        Debug.Log("Server has started");
    }

    public override void OnStopServer()
    {
        Debug.Log("Server has stopped");
    }

    public override void OnStopHost()
    {
        Debug.Log("Host has stopped");
    }

    // Client callbacks
    public override void OnClientConnect(NetworkConnection conn)
    {
        base.OnClientConnect(conn);
        Debug.Log("Connected successfully to server, now to set up other stuff for the client...");
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        Debug.Log("ON CLIENT DISCONNECT : " + conn.lastError);
        if (conn.lastError == NetworkError.Timeout)
        {
            Debug.Log("TIME OUT CONNECTING TO HOST, ATTEMPING TO RECONNECT AGAIN");

            //networkAddress = ipAddressInputField.text;
            // take the drive sim unity ip address from config file
            networkAddress = MiniLoader.Instance.ioCtrlAddr;
            Debug.Log("Starting Client : " + " ORIGINAL sNETWORK ADDRESS CONNECTING TO : " + networkAddress);
            StartClient();
        }
        else
        {
            StopClient();
            if (conn.lastError != NetworkError.Ok)
            {
                if (LogFilter.logError) { Debug.LogError("ClientDisconnected due to error: " + conn.lastError); }
            }
            Debug.Log("Client disconnected from server: " + conn);
        }

        
    }

    public override void OnClientError(NetworkConnection conn, int errorCode)
    {
        Debug.Log("Client network error occurred: " + (NetworkError)errorCode);
    }

    public override void OnClientNotReady(NetworkConnection conn)
    {
        Debug.Log("Server has set client to be not-ready (stop getting state updates)");
    }

    public override void OnStartClient(NetworkClient client)
    {
        Debug.Log("Client has started");
    }

    public override void OnStopClient()
    {
        Debug.Log("Client has stopped");
    }

    public override void OnClientSceneChanged(NetworkConnection conn)
    {
        base.OnClientSceneChanged(conn);
        Debug.Log("Server triggered scene change and we've done the same, do any extra work here for the client...");
    }
}