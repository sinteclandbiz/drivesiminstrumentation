﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UNetVarObjController : MonoBehaviour
{
    // the json variable to send across server
    public UNetVarObj uNetVarObj = new UNetVarObj();
    public static UNetVarObjController instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
    }

    public void UpdateUNetVarObj(DashboardVarObj dashboardVarObj)
    {
        uNetVarObj.dashboardVarObj = dashboardVarObj;
    }

    public void UpdateUNetVarObj(SwitchboardVarObj switchboardVarObj)
    {
        uNetVarObj.switchboardVarObj = switchboardVarObj;
    }

    public UNetVarObj GetUNetVarObj()
    {
        return uNetVarObj;
    }

    public string GetUNetVarJSONString()
    {
        return JSONSerializerController.instance.ConvertToJSONFormat<UNetVarObj>(uNetVarObj);
    }
}
