﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.Networking;

public class Player_SendMessageToServer : NetworkBehaviour
{
    public float moveSpeed = 1f;

    //public bool iAmHost = false;

    [SyncVar]
    public string syncClientToServerString;

    [SyncVar]
    public string syncServerToClientString;

    [SerializeField] Transform myTransform;
    [SerializeField] float lerpRate = 15f;

    private void Start()
    {
        DebugTextController.instance.AddDebugText("I SPAWNED");
        if (isLocalPlayer)
        {
            if (isServer && isClient)
            {
                DebugTextController.instance.AddDebugText("THIS IS HOST");
                this.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.yellow);
                this.name = "HostGO";

                // store on script drive sim unity
                if (NetworkHostController.instance != null)
                {
                    NetworkHostController.instance.RegisterHostGO(this.gameObject);
                }
            }

            else if (isClient && !isServer)
            {
                DebugTextController.instance.AddDebugText("THIS IS CLIENT");
                this.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.blue);
                this.name = "ClientGO";

                // store on script drive sim unity
                if (NetworkHostController.instance != null)
                {
                    NetworkHostController.instance.AddClientGameObject(this.gameObject);
                }


                //iAmHost = false;
            }
        }

        // i am not local player
        else
        {
            DebugTextController.instance.AddDebugText("I AM NOT LOCAL PLAYER");

            // this will be a confirmation that i am drive sim unity (host)
            if (NetworkHostController.instance != null && NetworkHostController.instance.CheckIfThereIsHostGO() == true)
            {
                DebugTextController.instance.AddDebugText("THIS IS CLIENT");
                this.GetComponent<MeshRenderer>().material.SetColor("_Color", Color.blue);
                this.name = "ClientGO";
                // so this object will be client
                NetworkHostController.instance.AddClientGameObject(this.gameObject);
            }
        }
    }

    public void OnDestroy()
    {
        if (isLocalPlayer)
        {
            DebugTextController.instance.AddDebugText("I AM A LOCAL PLAYER AND DESTROYED :" + this.gameObject.name);
        }
        else
        {
            DebugTextController.instance.AddDebugText("I AM NOT A LOCAL PLAYER AND DESTROYED :" + this.gameObject.name);

            // i am a client object and also on drive sim unity project
            if (NetworkHostController.instance != null && NetworkHostController.instance.CheckIfThereIsHostGO() == true)
            {
                // i remove myself on storeallnetworkvariablescontroller
                NetworkHostController.instance.RemoveClientGameObject(this.gameObject);
            }
        }
    }

    private void Update()
    {
        //// get my network gameobject details
        //if (Input.GetKeyDown(KeyCode.J))
        //{
        //    if (isLocalPlayer)
        //    {
        //        if (isServer && isClient)
        //        {
        //            DebugTextController.instance.AddDebugText("THIS IS HOST");

        //            // server connecting to clients will call this
        //            DebugTextController.instance.AddDebugText("THIS IS SERVER CONNECTING TO CLIENT : " + "THIS IS GO : " + this.gameObject.name + " LocalIPAddress: " + LocalIPAddress() + " IDENTIFIER : " + this.GetComponent<NetworkIdentity>().connectionToClient.connectionId);

        //            // transmit a message from host to all clients
        //            TransmitMessageToClient("SERVER TO CLIENT MY IP ADDRESS: " + LocalIPAddress());
        //        }

        //        else if (isClient && !isServer)
        //        {
        //            DebugTextController.instance.AddDebugText("THIS IS CLIENT");
        //            // client connecting to server will call this
        //            DebugTextController.instance.AddDebugText("THIS IS CLIENT CONNECTING TO SERVER : " + "THIS IS GO : " + this.gameObject.name + " LocalIPAddress: " + LocalIPAddress() + " IDENTIFIER : " + this.GetComponent<NetworkIdentity>().connectionToServer.connectionId);

        //            // transmit message to server
        //            TransmitMessageToServer("Client To Server MY IP ADDRESS: " + LocalIPAddress());
        //        }
        //    }
        //}

        //if (Input.GetKeyDown(KeyCode.P))
        //{
        //    if (!isLocalPlayer)
        //    {
        //        if (string.IsNullOrEmpty (syncServerToClientString) != true)
        //        {
        //            //DebugTextController.instance.AddDebugText("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);
        //            NetworkClientController.instance.DebugHostMessage("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);
        //        }
        //    }
        //}

        // controls the movement by keyboard if this gameobject is ours
        //if (isLocalPlayer)
        //{
        //    float horizontal = Input.GetAxis("Horizontal");
        //    float vertical = Input.GetAxis("Vertical");

        //    Vector3 movement = new Vector3(horizontal, 0f, vertical);
        //    transform.position += movement * Time.deltaTime * moveSpeed;
        //}
    }

    private void FixedUpdate()
    {
        // if it is not my gameobject
        if (!isLocalPlayer)
        {
            // i check if i am on instrumentation and server has sent me a message
            if (NetworkClientController.instance != null && string.IsNullOrEmpty(syncServerToClientString) != true)
            {
                //DebugTextController.instance.AddDebugText("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);
                //NetworkClientController.instance.DebugHostMessage("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);

                // i register myself to instrumentation as the host gameobject
                NetworkClientController.instance.RegisterHostGO(this.gameObject);
            }
        }

        // if it is my gameobject
        if (isLocalPlayer)
        {
            //if (isServer && isClient)
            // check if i am on drive sim unity
            if (NetworkHostController.instance != null && NetworkHostController.instance.sendThisMessageToClientsInputField != null)
            {
                // transmit a message from host to all clients
                TransmitMessageToClient("SERVER TO CLIENT MY IP ADDRESS: " + LocalIPAddress() + " WITH MESSAGE: " + NetworkHostController.instance.sendThisMessageToClientsInputField.text);
            }

            // check if i am on instrumentation project
            //else if (NetworkClientController.instance != null && isClient && !isServer)
            else if (NetworkClientController.instance != null && UNetVarObjController.instance != null && string.IsNullOrEmpty(UNetVarObjController.instance.GetUNetVarJSONString()) != null)
            {
                // transmit message to server
                TransmitMessageToServer(UNetVarObjController.instance.GetUNetVarJSONString());
            }
        }
    }

    public string GetServerMessageToClient()
    {
        if (!isLocalPlayer)
        {
            if (string.IsNullOrEmpty(syncServerToClientString) != true)
            {
                //DebugTextController.instance.AddDebugText("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);
                //NetworkClientController.instance.DebugHostMessage("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " + syncServerToClientString);

                return syncServerToClientString;
            }

            return null;
        }
        else
        {
            return null;
        }
    }

    //void LerpPosition()
    //{
    //    // supposed to be server synching the position across all clients?
    //    if (!isLocalPlayer)
    //    {
    //        myTransform.position = Vector3.Lerp(myTransform.position, syncString, Time.deltaTime * lerpRate);
    //    }
    //}

    // SERVER TO CLIENT
    [ClientRpc]
    void RpcProvideMessageToClient(string message)
    {
        syncServerToClientString = message;
    }

    [ServerCallback]
    void TransmitMessageToClient(string stringToPass)
    {
        if (isLocalPlayer)
        {
            RpcProvideMessageToClient(stringToPass);
        }
    }

    // CLIENT TO SERVER
    [Command]
    void CmdProvideMessageToServer(string message)
    {
        syncClientToServerString = message;
    }

    [ClientCallback]
    void TransmitMessageToServer(string stringToPass)
    {
        if (isLocalPlayer)
        {
            CmdProvideMessageToServer(stringToPass);
        }
    }

    public static string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "0.0.0.0";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }
}
