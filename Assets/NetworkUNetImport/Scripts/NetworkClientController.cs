﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using UnityEngine;
using UnityEngine.UI;

public class NetworkClientController : MonoBehaviour
{
    public InputField sendThisMessageToServerInputField;
    public GameObject HostGameObject;
    // the unet var object that server send to client
    public UNetVarObj uNetVarObj;
    public static NetworkClientController instance { get; private set; }

    private void Start()
    {
        instance = this;
    }

    public void RegisterHostGO(GameObject HostGO)
    {
        HostGameObject = HostGO;
    }

    private void Update()
    {
        if (HostGameObject != null)
        {
            // get server message to client json string and convert to uNetVarObject
            uNetVarObj = JSONSerializerController.instance.JSONToObject<UNetVarObj>(HostGameObject.GetComponent<Player_SendMessageToServer>().GetServerMessageToClient());
        }
    }

    public void DebugHostMessageButtonPressed()
    {
        if (HostGameObject != null)
        {
            DebugTextController.instance.AddDebugText("I AM A HOST GO ON A CLIENT MACHINE WITH THE MESSAGE : " +
                HostGameObject.GetComponent<Player_SendMessageToServer>().GetServerMessageToClient());
        }
    }

    public static string LocalIPAddress()
    {
        IPHostEntry host;
        string localIP = "0.0.0.0";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }
}
