﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JSONSerializerController : MonoBehaviour
{
    public static JSONSerializerController instance { get; private set; }
    void Awake()
    {
        instance = this;
    }

    public string ConvertToJSONFormat<T>(T classToPassIn)
    {
        string JSONFormatToReturn = JsonUtility.ToJson(classToPassIn, true);
        return JSONFormatToReturn;
    }

    public string ConvertToJSONFormat<T>(List<T> classToPassIn)
    {
        string JSONFormatToReturn = JsonHelper.ToJson(classToPassIn, true);
        return JSONFormatToReturn;
    }

    public T JSONToObject<T>(string JSON)
    {
        return JsonUtility.FromJson<T>(JSON);
    }

    public List<T> JSONToObjectList<T>(string JSON)
    {
        return JsonHelper.FromJson<T>(JSON);
    }
    // use this when getting JSON from server
    string fixJson(string value)
    {
        value = "{\"Items\":" + value + "}";
        return value;
    }
}
