﻿// for use with scdf ambulance siren controls
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SirenBoardController : MonoBehaviour
{
    // the parent for SirenControl
    public GameObject SirenControlPanelGO, SirenDetailPanelGO;
    // sub panels
    public GameObject SirenSideTurnOnPanelGO;

    // gameobjects under sirenSideTurnOnPanelGO
    public List<GameObject> AllSirenSideTurnOnSirenOnButtons = new List<GameObject>();
    public GameObject SirenSidenTurnOnTurnOffButton;
    // the root parent for sirent control
    public GameObject SirenControlParentPanelGO;
    public List<GameObject> AllSirenControlPanelButtons = new List<GameObject>();
    public List<GameObject> AllSirenDetailPanelButtons = new List<GameObject>();

    // various individual buttons
    public GameObject SirenLightButton, SirenLightAndSoundButton, RumbleButton, WailButton, YelpButton, HiLowButton, PiercerButton;
    public GameObject SirenSideTurnOnButton;
    public static SirenBoardController instance { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        instance = this;

        // disable siren control panel go
        SirenControlPanelGO.gameObject.SetActive(false);

        // disable siren detail control panel go
        SirenDetailPanelGO.gameObject.SetActive(false);

        // turn on siren control root panel go
        SirenControlParentPanelGO.gameObject.SetActive(true);
    }

    public void RumbleButtonPressed()
    {
        // if rumble is activated, change type of button for wail, yelp, hilow and piercer to rumble or not rumble
        if (RumbleButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
        {
            WailButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenWailRumbleSound;
            YelpButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenYelpRumbleSound;
            HiLowButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenHiLowRumbleSound;
            PiercerButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenPiercerRumbleSound;
        }
        else
        {
            WailButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenWailSound;
            YelpButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenYelpSound;
            HiLowButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenHiLowSound;
            PiercerButton.GetComponent<ButtonToggleUNET>().typeOfButton = ButtonToggleUNET.TypeOfButton.SCDFSirenPiercerSound;
        }
    }

    public void SirenLightControlPanelButtonPressed()
    {
        // turn on siren control panel go
        SirenControlPanelGO.gameObject.SetActive(true);

        // turn off siren control root panel go
        SirenControlParentPanelGO.gameObject.SetActive(false);

        // get all the buttons in siren light control panel
        foreach (GameObject button in AllSirenControlPanelButtons)
        {
            // change all to unselected buttons
            if (button.GetComponent<SpriteSwapOnPress>() != null)
            {
                button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
        }

        SirenLightButtonPressed();

        // turn off wail button
        WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();

        // turn off sound and light button
        SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
    }

    public void BackSirenDetailButtonPressed()
    {
        // turn off siren detail panel go
        SirenDetailPanelGO.gameObject.SetActive(false);

        // turn on siren control root panel go
        SirenControlParentPanelGO.gameObject.SetActive(true);

        // get all the buttons in siren light control panel
        foreach (GameObject button in AllSirenDetailPanelButtons)
        {
            // change all to unselected buttons
            if (button.GetComponent<SpriteSwapOnPress>() != null)
            {
                button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
        }
    }

    public void SirenLightAndSoundControlPanelButtonPressed()
    {
        // turn on siren control panel go
        SirenControlPanelGO.gameObject.SetActive(true);

        // turn off siren control root panel go
        SirenControlParentPanelGO.gameObject.SetActive(false);

        // get all the buttons in siren light control panel
        foreach (GameObject button in AllSirenControlPanelButtons)
        {
            // change all to unselected buttons
            if (button.GetComponent<SpriteSwapOnPress>() != null)
            {
                button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
        }

        SirenLightButtonPressed();
    
        // change sprite to selected
        SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
        WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

        // turn on wail button
        WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();

        // turn on sound and light button
        SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
    }

    public void GoToSirenDetailsFromSirenDetailButtonPressed()
    {
        Debug.Log("GO TO SIREN DETAIL BUTTON");
        // get all the buttons in siren light control panel
        foreach (GameObject button in AllSirenControlPanelButtons)
        {
            // change all to unselected buttons
            if (button.GetComponent<SpriteSwapOnPress>() != null)
            {
                button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
        }

        SirenControlPanelGO.SetActive(false);
        //SirenControlParentPanelGO.SetActive(false);
        SirenDetailPanelGO.SetActive(true);
    }

    public void SirenDetailsPanelButtonPressed()
    {
        Debug.Log("Siren Details Panel BUTTON Pressed");
        SirenControlPanelGO.SetActive(false);
        SirenControlParentPanelGO.SetActive(false);
        SirenDetailPanelGO.SetActive(true);

        //SirenSideTurnOnButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
    }

    public void NoSirenTurnOffButtonPressed()
    {
        //if (SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == false)
        //{
            // get all the buttons in siren light control panel
            foreach (GameObject button in AllSirenSideTurnOnSirenOnButtons)
            {
                // change all to unselected buttons
                if (button.GetComponent<SpriteSwapOnPress>() != null)
                {
                    button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
                }
            }
        //}
    }

    public void LSideSirenButtonPressed()
    {
        //SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
    }

    public void RSideSirenButtonPressed()
    {
        //SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
    }

    public void RearSideSirenButtonPressed()
    {
        //SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
    }

    public void LDSideSirenButtonPressed()
    {
        //SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
    }

    public void RDSideSirenButtonPressed()
    {
        //SirenSidenTurnOnTurnOffButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
    }

    public void SirenSideTurnOnButtonPressed()
    {
        //if (SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        //{
        //    SirenSideTurnOnPanelGO.SetActive(true);
        //}
    }

    public void SirenLightAndSoundButtonPressed()
    {
        if (SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // change light to selected
            // change light and sound to unselected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }

            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
        else
        {
            // change both light , light and sound button to selected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            //WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

            if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == false)
            {
                WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            }
            if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }



            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
    }

    public void SirenLightButtonPressed()
    {
        if (SirenLightButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // get all the buttons in siren light control panel
            foreach (GameObject button in AllSirenControlPanelButtons)
            {
                // change all to unselected buttons
                if (button.GetComponent<SpriteSwapOnPress>() != null)
                {
                    button.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
                }
                // change all to unselected buttons
                if (button.GetComponent<ButtonToggleUNET>() != null)
                {
                    button.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
                }
            }

            // turn off wail button
            WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();

            // turn off sound and light button
            SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();

            // turn off siren control panel go
            SirenControlPanelGO.gameObject.SetActive(false);

            // turn on siren control root panel go
            SirenControlParentPanelGO.gameObject.SetActive(true);
        }
        else
        {
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
        }
    }

    public void WailButtonSelected()
    {
        //WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
        if (WailButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // change light to selected
            // change light and sound to unselected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
                

            // change network variable to send across all the other buttons
            //if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            //{
            //    WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
            //}
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
        else
        {
            // change both light , light and sound button to selected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
            }

            // change all the others button if they are selected
            //if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            //{
            //    WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            //}
            if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }

            // change network variable to send across all the other buttons
            //if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            //{
            //    WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
    }

    public void YelpButtonSelected()
    {
        if (YelpButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // change light to selected
            // change light and sound to unselected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }


            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            //{
            //    YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
        else
        {
            // change both light , light and sound button to selected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
            }

            // change all the others button if they are selected
            if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            //if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            //{
            //    YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            //}
            if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }

            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            //{
            //    YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
    }

    public void HighBeamButtonSelected()
    {
    }

    public void HiLowButtonSelected()
    {
        if (HiLowButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // change light to selected
            // change light and sound to unselected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }


            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            //{
            //    HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
        else
        {
            // change both light , light and sound button to selected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
            }

            // change all the others button if they are selected
            if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            //if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            //{
            //    HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            //}
            if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }

            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            //{
            //    HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
            if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
        }
    }

    public void PiercerButtonSelected()
    {
        if (PiercerButton.GetComponent<SpriteSwapOnPress>().GetButtonSelectedOrNot() == true)
        {
            // change light to selected
            // change light and sound to unselected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }


            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            //{
            //    PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
        }
        else
        {
            // change both light , light and sound button to selected
            SirenLightButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();
            SirenLightAndSoundButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToSelected();

            if (SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                SirenLightAndSoundButton.GetComponent<ButtonToggleUNET>().CallButtonOnSelected();
            }

            // change all the others button if they are selected
            if (WailButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                WailButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (YelpButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                YelpButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            if (HiLowButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            {
                HiLowButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            }
            //if (PiercerButton.GetComponent<SpriteSwapOnPress>().buttonSelected == true)
            //{
            //    PiercerButton.GetComponent<SpriteSwapOnPress>().ChangeSpriteToUnSelected();
            //}

            // change network variable to send across all the other buttons
            if (WailButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == true)
            {
                WailButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (YelpButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                YelpButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            if (HiLowButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            {
                HiLowButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            }
            //if (PiercerButton.GetComponent<ButtonToggleUNET>().TurnOnWhenPress == false)
            //{
            //    PiercerButton.GetComponent<ButtonToggleUNET>().CallButtonOnUnselected();
            //}
        }
    }

    public void AmbulanceSirenButtonSelected()
    {
    }
}
