﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class ButtonGear : Button
{
    public ButtonGear[] buddyObjects;
    public bool isOn;
    public bool isBatteryOn;
    public int batteryButtonNumber;

    private ulong batteryMask;
    private EventTrigger trigger;
    private bool triggered;
    private Image imageEnabled, imageDisabled;
    // Use this for initialization
    public new void Start ()
    {
        base.Start();

        batteryMask = (one << batteryButtonNumber);

        isOn = false;

        triggered = false;

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 2)
        {
            imageEnabled = tempImg[0];
            imageDisabled = tempImg[1];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 2 image laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);
    }

    public void GearSwitch()
    {
        if (isOn)
        {
            imageEnabled.gameObject.SetActive(!imageEnabled.gameObject.activeSelf);
            imageDisabled.gameObject.SetActive(!imageEnabled.gameObject.activeSelf);
        }
    }

    // Update is called once per frame
    public  void Update ()
    {
        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        if (isBatteryOn == false)
        {
            DefaultSettings();
        }
    }

    public void OnDestroy()
    {
        trigger.triggers.Clear();
        trigger = null;
        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler -= HandleReceiveFromPLC;
    }

    public void Flip()
    {
        isOn = false;
        base.SentToPLC();
        DefaultSettings();
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click for " + gameObject.name + ", now sending!");
            triggered = true;
            isOn = true;
            for (int i = 0; i < buddyObjects.Length; i++)
            {
                if (buddyObjects[i].isOn)
                {
                    buddyObjects[i].Flip();
                }
            }
            GearSwitch();

            base.SentToPLC();
        }
    }

    public void DefaultSettings()
    {
        imageEnabled.gameObject.SetActive(false); 
        imageDisabled.gameObject.SetActive(true);
        isOn = false;
    }
}
