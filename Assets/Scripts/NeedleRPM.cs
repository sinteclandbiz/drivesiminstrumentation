﻿public class NeedleRPM : Needle
{ 
    protected  override void SetValue()
    {
        // contains the tabulated difference of the actual speed/rpm send by simhost/plc
        targetNumber = (float)gauge.rpm / 100;
    }
}
