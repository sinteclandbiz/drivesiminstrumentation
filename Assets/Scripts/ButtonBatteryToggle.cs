﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;
/// <summary>
/// This class is the big f***, if it doesn't get turned on, nobody gets turned on!
/// </summary>

// The GameObject requires a component
[RequireComponent(typeof(EventTrigger))]
public class ButtonBatteryToggle : Button
{
    public int neutralGearNumber;
    public ButtonGear neutral;
    public ButtonRotaryToggle rotaryNeural;
    public bool sentNeutralBytesToo;
    public TextHelpListenFromButton txtHelp;

    private ulong neutralGearMask;
    private byte[] neutralGearByte;
    private Image bottomImageEnable, bottomImageDisable, topImageEnable, topImageDisable;
    private EventTrigger trigger;
    private bool triggered;
    private bool isOn;
    private IEnumerator sentAfterSomeTimeCoroutine;
    private const int TIMERNUMBER = 1;

    public new void Start()
    {
        base.Start();

        isOn = false;

        neutralGearMask = (one << neutralGearNumber);

        neutralGearByte = BitConverter.GetBytes(neutralGearMask);

        triggered = false;

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 4)
        {
            bottomImageEnable = tempImg[0];
            bottomImageDisable = tempImg[1];
            topImageEnable = tempImg[2];
            topImageDisable = tempImg[3];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 4 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);

        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler += HandleReceiveFromPLC;
        //StartCoroutine("CheckToTurnOff");
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (triggered)
        {
            bottomImageEnable.gameObject.SetActive(!bottomImageEnable.gameObject.activeSelf);
            bottomImageDisable.gameObject.SetActive(!bottomImageDisable.gameObject.activeSelf);
            topImageEnable.gameObject.SetActive(!topImageEnable.gameObject.activeSelf);
            topImageDisable.gameObject.SetActive(!topImageDisable.gameObject.activeSelf);
            triggered = false;
        }
    }

    public  void Update()
    {
        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isMatch)
        {
            //Logger.Log("Matched! " + gameObject.name + " is flipping now!");

            HandleReceiveFromPLC();
        }
    }

    public void OnDestroy()
    {
        trigger.triggers.Clear();
        trigger = null;
    }

    public override void OnClickSent(BaseEventData arg)
    {
        Logger.Log("Successfully registered click, now sending!");
        base.SentToPLC();
        
        if(txtHelp != null)
        {
            txtHelp.DisplayOff();
        }

        triggered = true;

        isOn = !isOn;

        if(isOn)
        {
            if (neutral != null)
            {
                neutral.isOn = true;

                if (sentNeutralBytesToo && neutral.isOn)
                {
                    //SentNeutralBytes();
                    InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);

                }
            }

            if (rotaryNeural != null)
            {
                rotaryNeural.isOn = true;

                if (sentNeutralBytesToo && rotaryNeural.isOn)
                {
                    //SentNeutralBytes();
                    InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);

                }
            }
        }

        if (isOn == false)
        {
            Logger.Log("Turning off! " + gameObject.name + "is flipping now!");

            if (neutral != null)
            {
                neutral.isOn = false;
            }

            if (rotaryNeural != null)
            {
                rotaryNeural.isOn = false;
            }

            InstrumentController.Instance.buttonController.SentDefault();

            if (txtHelp != null)
            {
                txtHelp.DefaultSettings();
            }

            DefaultSettings();
        }
    }

    public void SentNeutralBytes()
    {
        if(sentAfterSomeTimeCoroutine == null)
        {
            sentAfterSomeTimeCoroutine = SentAfterSomeTime();
            StartCoroutine(sentAfterSomeTimeCoroutine);
        }
    }

    public IEnumerator SentAfterSomeTime()
    {
        yield return new WaitForSeconds(TIMERNUMBER);

        InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);

        StopCoroutine(sentAfterSomeTimeCoroutine);
        sentAfterSomeTimeCoroutine = null;
    }

    public void DefaultSettings()
    {
        bottomImageEnable.gameObject.SetActive(false);
        bottomImageDisable.gameObject.SetActive(true);
        topImageEnable.gameObject.SetActive(false);
        topImageDisable.gameObject.SetActive(true);
        triggered = false;
        sentAfterSomeTimeCoroutine = null;

        if (neutral != null)
        {
            neutral.isOn = false;
        }

        if (rotaryNeural != null)
        {
            rotaryNeural.isOn = false;
        }
    }

    public bool getBatteryStatus()
    {
        return isOn;
    }
}
