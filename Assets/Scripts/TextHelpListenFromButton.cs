﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextHelpListenFromButton : MonoBehaviour
{
	// Use this for initialization
	void Start ()
    {
        gameObject.SetActive(true);
    }

    public void DisplayOff()
    {
        gameObject.SetActive(false);
    }

    public void DefaultSettings()
    {
        gameObject.SetActive(true);
    }
}
