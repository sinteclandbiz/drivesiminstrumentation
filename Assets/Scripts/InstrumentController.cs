﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Net;

public class InstrumentController : MonoBehaviour
{
    private static InstrumentController _instance = null;
    private static Object _lock = new Object();

    public PLCMaster plcMaster;
    public PLCSender plcSender = null;
    public ButtonController buttonController;
    public string plcAdd;
    public int plcPort, plcAckPort;
    public string plcSentAdd;
    public int plcSentPort;
    public bool isHelp;

    public static InstrumentController Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<InstrumentController>();
                    if (_instance == null)
                    {
                        Debug.LogError("InstrumentController Script must be attached to a GameObject");
                    }
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }
    }

    void Awake()
    {
        buttonController = new ButtonController();      

        if(MiniLoader.Instance.isSwitch)
        {
            //if you are a switch, you will listen on local IP and port 5000, the port that dashboard sents to and is
            // your "PLC"
            plcPort = 5000;
        }
        else
        {
            plcPort = MiniLoader.Instance.plcPort;

        }

        plcAdd = MiniLoader.Instance.ioCtrlAddr;

        plcAckPort = MiniLoader.Instance.plcAckPort;

        isHelp = MiniLoader.Instance.DashboardShowHelpText;

        plcMaster = new PLCMaster(plcAdd, plcPort, plcAckPort);

        //SUPER IMPORTANT! COMMENT IF STATEMENT WHEN TESTING IN UNITY EDITOR AND UNCOMMENT WHEN DEPLOYING INTO THE WILD
        // this is to set the ports for sending data to PLC! comment only if you wanna be dashboard only!
        //if (MiniLoader.Instance.isSwitch)
        //{
        //    plcSender.controllerPort = MiniLoader.Instance.DashboardSendToPort;
        //    plcSender.port = MiniLoader.Instance.DashboardSendToPort;
        //    plcSender.controllerAddress = MiniLoader.Instance.ioCtrlAddr;
        //}

        Debug.Log("I AM AWAKE! THIS IS THE INSTRUMENT CONTROLLER ROCKIN");
    }

    public void Update()
    {
        plcMaster.Update();
    }

    public void OnDestroy()
    {
        plcMaster.Stop();
        plcSender.Stop();
    }
}
