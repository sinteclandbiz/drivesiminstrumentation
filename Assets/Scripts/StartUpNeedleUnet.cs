﻿// use this for starting up a needle on ignition

using System.Collections;
using UnityEngine;

public class StartUpNeedleUnet : NeedleUnet
{
    public float targetNumberMin = 0f, targetNumberMax;
    public float turnOnDelayTimer = 4f;
    private float targetNumberModifier = 1;
    private float lerpTargetNumber = 0f;

    public override void Start()
    {
        base.Start();

        targetNumber = targetNumberMin;
    }

    public override void Update()
    {
        base.Update();

        if (Input.GetKeyDown(KeyCode.H))
        {
            StartUpNeedleAnimation();
        }
    }

    public void StartUpNeedleAnimation()
    {
        StartCoroutine(LerpFunction(targetNumberMax, turnOnDelayTimer));
    }

    IEnumerator LerpFunction(float endValue, float duration)
    {
        float time = 0;
        float startValue = targetNumberModifier;
        //Vector3 startScale = transform.localScale;

        while (time < duration)
        {
            targetNumberModifier = Mathf.Lerp(targetNumberMin, endValue, time / duration);
            lerpTargetNumber = targetNumberModifier;
            time += Time.deltaTime;
            yield return null;
        }
        targetNumberModifier = targetNumberMax;
    }

    protected override void SetValue()
    {
        // contains the tabulated difference of the actual speed/rpm send by simhost/plc
        targetNumber = lerpTargetNumber;
    }
}