﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextHelpListenFromPLC : MonoBehaviour
{
    public int bitPositionNumber;
    public bool doNotShowAgain;
    public int batteryBitPositionNumber;
    public Text text;
    public bool isBatteryOn;

    private ulong mask;
    private ulong mask2;
    private ulong one; 
    private bool showAgain;
    private bool isMatch; 
    
    // Use this for initialization
    void Start ()
    {
        one = 1;

        mask = (one << bitPositionNumber);

        mask2 = (one << batteryBitPositionNumber);

        showAgain = true;
    }
	
	// Update is called once per frame
	public void Update ()
    {
        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask) == mask;

        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false && isMatch == false)
        {
            DefaultSettings();
        }

        if (isMatch && isBatteryOn)
        {
            DisplayOff();        
        }
    }

    public void DisplayOff()
    {
        text.enabled = false;
    }

    public void DefaultSettings()
    {
        text.enabled = true;
        showAgain = true;
    }
}
