﻿using UnityEngine;
/// <summary>
/// base class for speedometer and RPM
/// </summary>
public class Needle : MonoBehaviour
{
    public float minNumber, maxNumber;
    public float targetNumber;
    public float minimumRotation;
    public float maximumRotation;

    private float regulatedNumber; // contains the tabulation of the actual rotation result
    private RectTransform needle;
    private GameObject gaugeReceiver;

    protected GaugeReceiver gauge;
    // Use this for initialization
    void Start ()
    {
        gaugeReceiver = GameObject.Find("/GaugeReciever");

        needle = gameObject.GetComponent<RectTransform>();

        gauge = gaugeReceiver.gameObject.GetComponent<GaugeReceiver>();

        regulatedNumber = 0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        SetValue();

        regulatedNumber = CalculateRotation(targetNumber);
        Debug.Log("Regulated number : "+regulatedNumber);
        needle.localRotation = Quaternion.Euler(new Vector3(0f, 0f, regulatedNumber));
	}

    private float CalculateRotation(float number)
    {
        return minimumRotation - ((number - minNumber) / (maxNumber - minNumber)) * maximumRotation;
    } 

    protected virtual void SetValue()
    {
        // contains the tabulated difference of the actual speed/rpm send by simhost/plc
    }
}
