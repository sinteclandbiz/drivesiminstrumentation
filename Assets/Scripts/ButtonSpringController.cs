﻿using UnityEngine;
using System;

public class ButtonSpringController : MonoBehaviour
{
    public ButtonSpringTop topButton;
    public ButtonSpringBottom bottomButton;
    public int topButtonNumber;
    public int topButtonExpectedNumber;
    public int bottomButtonNumber;
    public int bottomButtonExpectedNumber;
    public string currentButtonPressed;

    private string top = "Top";
    private string bottom = "Bottom";
    private ulong one = 1;

    private bool topTriggered;
    private bool bottomTriggered;

    private byte[] topButtonByteArray { get; set; }

    private byte[] topButtonByteArray2 { get; set; }

    private ulong topButtonMask { get; set; }

    private ulong topButtonMask2 { get; set; }

    private bool isMatchForTopButton { get; set; }



    private byte[] bottomButtonByteArray { get; set; }

    private byte[] bottomButtonByteArray2 { get; set; }

    private ulong bottomButtonMask { get; set; }

    private ulong bottomButtonMask2 { get; set; }

    private bool isMatchForBottomButton { get; set; }

    // Use this for initialization
    void Start ()
    {
        isMatchForTopButton = false;

        isMatchForBottomButton = false;

        topButtonMask = (one << topButtonNumber);
        topButtonByteArray = BitConverter.GetBytes(topButtonMask);
        topButtonMask2 = (one << topButtonExpectedNumber);
        topButtonByteArray2 = BitConverter.GetBytes(topButtonMask2);

        bottomButtonMask = (one << bottomButtonNumber);
        bottomButtonByteArray = BitConverter.GetBytes(bottomButtonMask);
        bottomButtonMask2 = (one << bottomButtonExpectedNumber);
        bottomButtonByteArray2 = BitConverter.GetBytes(bottomButtonMask2);
    }
	
	// Update is called once per frame
	void Update ()
    {
        isMatchForTopButton = (InstrumentController.Instance.plcMaster.switchesState & topButtonMask2) == topButtonMask2;

        isMatchForBottomButton = (InstrumentController.Instance.plcMaster.switchesState & bottomButtonMask2) == bottomButtonMask2;

        if (isMatchForTopButton && currentButtonPressed.Equals(top))
        {
            //do button flip
            HandleFromPLCForTopButton();
        }

        if (isMatchForBottomButton && currentButtonPressed.Equals(bottom))
        {
            //do button flip
            HandleFromPLCForBottomButton();
        }
    }

    public void HandleFromPLCForTopButton()
    {
        if(topTriggered)
        {
            topButton.FlipActivatedBySelf();
            bottomButton.FlipActivatedByOthers();
        }
        if (topTriggered == false)
        {
            topButton.DefaultSettings();
            bottomButton.DefaultSettings();
        }
    }

    public void HandleFromPLCForBottomButton()
    {
        if (bottomTriggered)
        {
            topButton.FlipActivatedByOthers();
            bottomButton.FlipActivatedBySelf();
        }

        if (bottomTriggered == false)
        {
            topButton.DefaultSettings();
            bottomButton.DefaultSettings();
        }
    }

    public void SetTopTrigger(bool trigger, string buttonType)
    {
        currentButtonPressed = buttonType;

        if(trigger)
        {
            topTriggered = true;

            InstrumentController.Instance.buttonController.MergeForSending(topButtonByteArray);
        }
        
        if(trigger == false)
        {
            topTriggered = false;

            InstrumentController.Instance.buttonController.MergeForSending(topButtonByteArray);
        }
    }

    public void SetBottomTrigger(bool trigger, string buttonType)
    {
        currentButtonPressed = buttonType;

        if (trigger)
        {
            bottomTriggered = true;

            InstrumentController.Instance.buttonController.MergeForSending(bottomButtonByteArray);
        }

        if (trigger == false)
        {
            bottomTriggered = false;

            InstrumentController.Instance.buttonController.MergeForSending(bottomButtonByteArray);
        }
    }
}
