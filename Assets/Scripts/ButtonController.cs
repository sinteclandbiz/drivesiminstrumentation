﻿using System;
/// <summary>
/// the bread and butter for all things that require to sent bytes to PLC
/// </summary>
public class ButtonController
{
    public byte[] mergedBytes;
    public byte[] neutral;

    public ButtonController()
    {
        mergedBytes = new byte[6];
        neutral = new byte[8];
    }

    public void SentDefault()
    {
        for (int i = 0; i < mergedBytes.Length; i++)
        {
            mergedBytes[i] = 0;
            mergedBytes[i] = (byte)(mergedBytes[i] ^ neutral[i]);            
        }
    }

    //combine the values in mergedBytes and incoming data together before sending to PLC
    public void MergeForSending(byte[] data)
    {
        for (int i = 0; i < mergedBytes.Length; i++)
        {
            mergedBytes[i] = (byte)(mergedBytes[i] ^ data[i]);
        }

        InstrumentController.Instance.plcSender.Send(mergedBytes);
    }
}
