﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

// The GameObject requires a component
[RequireComponent(typeof(EventTrigger))]
public class ButtonToggle : Button
{
    public bool isBatteryOn;
    public int batteryButtonNumber;
    public int neutralGearNumber;
    public ButtonGear neutral;

    private Image bottomImageEnable, bottomImageDisable, topImageEnable, topImageDisable;
    private EventTrigger trigger;
    private bool triggered;
    private ulong batteryMask;
    private ulong neutralGearMask;
    private byte[] neutralGearByte;
    public bool sentNeutralBytesToo;

    public new void Start()
    {
        base.Start();

        sentNeutralBytesToo = false;

        triggered = false;

        batteryMask = (one << batteryButtonNumber);

        neutralGearMask = (one << neutralGearNumber);

        neutralGearByte = BitConverter.GetBytes(neutralGearMask);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 4)
        {
            bottomImageEnable = tempImg[0];
            bottomImageDisable = tempImg[1];
            topImageEnable = tempImg[2];
            topImageDisable = tempImg[3];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 4 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);

        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler += HandleReceiveFromPLC;
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if(triggered)
        {
            bottomImageEnable.gameObject.SetActive(!bottomImageEnable.gameObject.activeSelf);
            bottomImageDisable.gameObject.SetActive(!bottomImageDisable.gameObject.activeSelf);
            topImageEnable.gameObject.SetActive(!topImageEnable.gameObject.activeSelf);
            topImageDisable.gameObject.SetActive(!topImageDisable.gameObject.activeSelf);

            triggered = false;

            //Logger.Log("It's a match for " + gameObject.name + "! Received from PLC : " + PLCMaster.ToBin(InstrumentController.Instance.plcMaster.switchesState, 48));
        }
    }

    public void Update()
    {
        //check if battery is on
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false)
        {
            DefaultSettings();
        }

        if (isMatch)
        {
            HandleReceiveFromPLC();
        }
    }

    public void SentNeutralBytes()
    {
        StartCoroutine("SentAfterOneSec");
    }

    public IEnumerator SentAfterOneSec()
    {
        yield return new WaitForSeconds(1);

        InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);
    }

    public void OnDestroy()
    {
        trigger.triggers.Clear();
        trigger = null;
        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler -= HandleReceiveFromPLC;
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");
            base.SentToPLC();
            triggered = true;
        }

        if (sentNeutralBytesToo)
        {
            neutral.isOn = true;
            StartCoroutine("SentAfterOneSec");
        }
    }

    public void DefaultSettings()
    {
        bottomImageEnable.gameObject.SetActive(false);
        bottomImageDisable.gameObject.SetActive(true);
        topImageEnable.gameObject.SetActive(false);
        topImageDisable.gameObject.SetActive(true);
        triggered = false;

        if(sentNeutralBytesToo)
        {
            neutral.isOn = false;
        }
    }
}
