﻿/**
 * ABB Sender - send UDP packet to AAB controller and retry sending until packet is ack
 * @author Tan Hock Woo <tanhw@stee.stengg.com>

 This class will sent whatever button presses from switch to PLC
 */
using UnityEngine;
using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class PLCSender : MonoBehaviour
{
    //if (Display.displays.Length > 1) (use for the mini loader)	
	
	//controllerAddress & controllerPort is for target IP i am sending to
    public string controllerAddress = "127.0.0.1";  // where is the controller located
    public int controllerPort = 2224;

    public string bindAddress = "0.0.0.0";			// our own local IP to bind to
    //public int port = 2225;
    public int port = 3030;
    public bool shouldResend = false;

    public float resendInterval = 0.1f;

    private IPEndPoint unicastEndPoint;
    private IPAddress bindIpAddress;

    private volatile Socket socket;
    private Thread senderThread;
	private Thread resendThread;
    private volatile bool runSender;

    private volatile byte[] lastPacket;
    private volatile object _lock = new object();
    private string data = "";

    // Use this for initialization
    //public void Start(string plcAdd, int plcPort)
    public void Start()
    {
        bindIpAddress = IPAddress.Parse(bindAddress);
        //unicastEndPoint = new IPEndPoint(IPAddress.Parse(controllerAddress), controllerPort);

        unicastEndPoint = new IPEndPoint(IPAddress.Parse(controllerAddress), controllerPort);

        socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        socket.ExclusiveAddressUse = false;
        socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        socket.Bind(new IPEndPoint(bindIpAddress, port));

        if (shouldResend)
        {
            runSender = true;
            senderThread = new Thread(Sender);
            senderThread.IsBackground = true;
            senderThread.Start();
			
			//resendThread = new Thread(Resend);
			//resendThread.IsBackground = true;
			//resendThread.Start();		
			
            StartCoroutine(Resend());
        }
    }

    public void OnDestroy()
    {
        Stop();
    }

    public void Stop()
    {
        runSender = false;

        if (socket != null)
        {
            Logger.LogInfo(string.Format("Shutting down AbbSender Socket bound to {0}:{1}", bindIpAddress, port));
            if (socket.Connected) { socket.Shutdown(SocketShutdown.Both); }
            socket.Close();
            Logger.LogInfo(string.Format("Successfully shut down AbbSender Socket bound to {0}:{1}", bindIpAddress, port));
        }
        // Check if there is a senderThread because it might not have been started at all
        if (senderThread != null && senderThread.IsAlive)
        {
            Logger.LogInfo("Sender Thread in AbbSender is still alive");
            senderThread.Interrupt();
            runSender = false;
            senderThread.Join();
            Logger.LogInfo("Sender Thread in AbbSender has joined");
        }
		
		if (resendThread != null && resendThread.IsAlive)
        {
            Logger.LogInfo("Resend Thread in AbbSender is still alive");
            resendThread.Interrupt();
            runSender = false;
            resendThread.Join();
            Logger.LogInfo("Resend Thread in AbbSender has joined");
        }
    }

    /// <summary>
    /// Send some stuff to PLC
    /// </summary>
    /// <param name="data">data to send to plc</param>
    public void Send(byte[] data)
    {
        socket.SendTo(data, unicastEndPoint);

        Logger.Log("Sending "+ BinToString(data) + " to PLC.");

        lock (_lock)
        {
            lastPacket = data;  // wait for this packet to be received
        }
    }

    public string BinToString(byte[] data)
    {
        string result = "";

        foreach (byte b in data)
        {
            int mask = 0x01;
            for (int n = 0; n < 8; ++n)
            {
                result = (((b & mask) == mask) ? "1" : "0") + result;
                mask <<= 1;
            }
        }

        return result;
    }


    private IEnumerator Resend()
    {
        while (runSender)
        {
            yield return new WaitForSeconds(resendInterval);
            lock (_lock)
            {
                if (lastPacket != null)
                {
                    //Logger.Log("Resending... ");
                    socket.SendTo(lastPacket, unicastEndPoint);
                }
            }
        }
    }

 //   private void Resend()
	//{
	//	while(runSender)
	//	{
	//		lock (_lock)
 //           {
 //               if (lastPacket != null)
 //               {
 //                   //Logger.Log("Resending... ");
 //                   socket.SendTo(lastPacket, unicastEndPoint);
 //               }
 //           }
	//	}
	//}
	
	//constantly receives from sender  
    private void Sender()
    {
        Logger.Log(string.Format("[AbbSender] Started - Thread {0}", senderThread.ManagedThreadId));

        byte[] buffer = new byte[4096];
        EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

        while (runSender)
        {
            try
            {
                int len = socket.ReceiveFrom(buffer, ref remoteEndPoint);

                // Receive data packet from IO Controller 
                lock (_lock)
                {
                    if (lastPacket == null || lastPacket.Length != len)
                    {
                        continue; // not for us!
                    }

                    bool isMatching = true;
                    for (int n = 0; n < len; ++n)
                    {
                        if (buffer[n] != lastPacket[n])
                        {
                            isMatching = false;
                            break;
                        }
                    }
                    if (isMatching)
                    {
                        lastPacket = null;
                        Logger.Log("Received ACK");
                    }
                }
            }
            catch (SocketException se)
            {
                // ignore error due to remote socket closed.
                Logger.LogError(se.ToString());
            }
            catch (ObjectDisposedException ode)
            {
                Logger.LogError(ode.ToString());
                runSender = false;
            }
        }

        Logger.Log(string.Format("[AbbSender] Stopped - Thread {0}", senderThread.ManagedThreadId));
    }
}
