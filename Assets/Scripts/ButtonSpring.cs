﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class ButtonSpring : Button
{        
    public bool isBatteryOn;
    public int batteryButtonNumber;
    public TextHelpListenFromButton txtHelp;
    public int neutralGearNumber;
    public ButtonGear neutral;
    public bool sentNeutralBytesToo;

    private EventTrigger trigger;
    private ulong batteryMask;
    private bool isOn;
    private Image bottomImageEnable, bottomImageDisable, topImageEnable, topImageDisable;
    private bool triggered;
    private ulong neutralGearMask;
    private byte[] neutralGearByte;

    public new void Start ()
    {
        base.Start();

        batteryMask = (one << batteryButtonNumber);

        Image[] tempImg = GetComponentsInChildren<Image>();

        neutralGearMask = (one << neutralGearNumber);

        neutralGearByte = BitConverter.GetBytes(neutralGearMask);

        if (tempImg.Length == 4)
        {
            bottomImageEnable = tempImg[0];
            bottomImageDisable = tempImg[1];
            topImageEnable = tempImg[2];
            topImageDisable = tempImg[3];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 4 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();

        EventTrigger.Entry entry1 = new EventTrigger.Entry();
        entry1.eventID = EventTriggerType.PointerDown;
        entry1.callback.AddListener(OnButtonDown);
        trigger.triggers.Add(entry1);

        EventTrigger.Entry entry2 = new EventTrigger.Entry();
        entry2.eventID = EventTriggerType.PointerUp;
        entry2.callback.AddListener(OnButtonUp);
        trigger.triggers.Add(entry2);
    }

    // Update is called once per frame
    public void Update ()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false)
        {
            DefaultSettings();
        }

        //if (isMatch)
        //{
        //    HandleReceiveFromPLC();
        //}
    }
    protected override void HandleReceiveFromPLC()
    {
        if (triggered)
        {
            bottomImageEnable.gameObject.SetActive(true);
            bottomImageDisable.gameObject.SetActive(false);
            topImageEnable.gameObject.SetActive(true);
            topImageDisable.gameObject.SetActive(false);
        }
        else
        {
            DefaultSettings();
        }
    }

    public void OnButtonDown(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");

            base.SentToPLC();

            if (txtHelp != null)
            {
                txtHelp.DisplayOff();
            }

            if (sentNeutralBytesToo)
            {
                neutral.isOn = true;

                InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);

                StartCoroutine("SentAfterSomeTime");
            }

            triggered = true;

            HandleReceiveFromPLC();
        }
    }

    public void OnButtonUp(BaseEventData arg)
    {
        base.SentToPLC();
        triggered = false;

        HandleReceiveFromPLC();
    }

    public void DefaultSettings()
    {
        bottomImageEnable.gameObject.SetActive(false);
        bottomImageDisable.gameObject.SetActive(true);
        topImageEnable.gameObject.SetActive(false);
        topImageDisable.gameObject.SetActive(true);

        if (txtHelp != null && isBatteryOn == false)
        {
            txtHelp.DefaultSettings();
        }

        if (neutral != null)
        {
            neutral.isOn = false;
        }
    }

}
