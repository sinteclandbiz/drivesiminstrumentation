﻿public class NeedleSpeedometerUnet : NeedleUnet
{
    protected override void SetValue()
    {
        // contains the tabulated difference of the actual speed/rpm send by simhost/plc
        targetNumber = (float)NetworkClientController.instance.uNetVarObj.dashboardVarObj.speed;
    }
}
