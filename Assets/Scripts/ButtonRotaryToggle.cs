﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

// The GameObject requires a component
[RequireComponent(typeof(EventTrigger))]
public class ButtonRotaryToggle : Button
{
    public GameObject dialObject;
    public ButtonRotaryToggle[] buddyObjects;
    public bool isBatteryOn;
    public int batteryButtonNumber;
    public float defaultZRotation;
    public float rotationValue;
    public bool isOn;
    public bool noNeedToSentToPLC;

    private Image image;

    private EventTrigger trigger;
    private ulong batteryMask;
    private bool triggered;

    public new void Start()
    {
        isOn = false;

        triggered = false;

        base.Start();

        //isBatteryOn = true;// set it to true for testing

        batteryMask = (one << batteryButtonNumber);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 1)
        {
            image = tempImg[0];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 1 image laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);

        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler += HandleReceiveFromPLC;
    }

    //protected override void HandleReceiveFromPLC()
    //{
    //    Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
    //    if (isOn)
    //    {
    //        if ((transform.rotation.eulerAngles.z + (rotationValue)) > rotationValue)
    //        {
    //            Debug.Log("Cannot rotate past limits la.");
    //        }
    //        else
    //        {
    //            dialObject.gameObject.transform.Rotate(new Vector3(0, 0, 1), rotationValue);
    //        }
    //        triggered = false;

    //        isOn = true;
    //    }
    //}

    public void Turn()
    {
        Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (isOn)
        {
            //dialObject.gameObject.transform.Rotate(new Vector3(0, 0, 1), rotationValue);

            dialObject.gameObject.transform.localEulerAngles = new Vector3(0, 0, rotationValue);
            //triggered = false;

            //isOn = true;
        }
    }

    public void Update()
    {
        //check if battery is on
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;


        if (isBatteryOn == false)
        {
            DefaultSettings();
        }

        //if (isMatch)
        //{
        //    Logger.Log("Turning on & matched! " + gameObject.name + " is fipping now!");

        //    HandleReceiveFromPLC();
        //}
    }

    public void OnDestroy()
    {
        trigger.triggers.Clear();
        trigger = null;
        //maybe implemented wrongly, comment for now
        //ReceivedFromPLCHandler -= HandleReceiveFromPLC;
    }

    public void Flip()
    {
        if (noNeedToSentToPLC == false)
        {
            base.SentToPLC();
        }

        isOn = false;
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click for " + gameObject.name + ", now sending!");
            triggered = true;
            isOn = true;
            for (int i = 0; i < buddyObjects.Length; i++)
            {
                if (buddyObjects[i].isOn)
                {
                    buddyObjects[i].Flip();
                }
            }

            Turn();

            if(noNeedToSentToPLC == false)
            {
                base.SentToPLC();
            }
        }

        //isOn = true;
        //for (int i = 0; i < buddyObjects.Length; i++)
        //{
        //    if (buddyObjects[i].isOn)
        //    {
        //        buddyObjects[i].Flip();
        //    }
        //}
        //Turn();

        //Debug.Log("Click and rotate!");

        //dialObject.gameObject.transform.localEulerAngles = new Vector3(0, 0, rotationValue);

        //below codes are for quick testing
        //dialObject.gameObject.transform.localEulerAngles = new Vector3(0, 0, rotationValue);
        //dialObject.gameObject.transform.Rotate(new Vector3(0, 0, 1), rotationValue);

        //if((dialObject.gameObject.transform.rotation.eulerAngles.z + (rotationValue)) > rotationValue)
        //{
        //    Debug.Log("Cannot rotate past limits la.");
        //}
        //else
        //{
        //    dialObject.gameObject.transform.Rotate(new Vector3(0, 0, 1), rotationValue);
        //}
    }

    public void DefaultSettings()
    {
        dialObject.transform.eulerAngles = new Vector3(0, 0, defaultZRotation);
        triggered = false;
    }
}
