﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class TextHelpForHandBrake : MonoBehaviour
{
    public int parkingBrakeBitPositionNumber;
    public int batteryBitPositionNumber;
    public bool doNotShowAgain;
    public Text text;

    private ulong mask;
    private ulong mask2;
    private ulong one;
    private bool showAgain;
    private bool isOn;
    public bool isBatteryOn;
    // Use this for initialization
    void Start()
    {
        one = 1;

        mask = (one << parkingBrakeBitPositionNumber);

        mask2 = (one << batteryBitPositionNumber);

        showAgain = true;
    }

    // Update is called once per frame
    void Update()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false)
        {
            DefaultSettings();
        }

        if (isBatteryOn)
        {
            if ((InstrumentController.Instance.plcMaster.switchesState & mask) == mask)
            {
                isOn = true;
            }
        }

        if (isOn && ((InstrumentController.Instance.plcMaster.switchesState & mask) != mask))
        {
            DisplayOff();
        }
    }

    public void DisplayOff()
    {
        text.enabled = false;
    }

    public void DefaultSettings()
    {
        text.enabled = true;
        showAgain = true;
        isOn = false;
    }
}
