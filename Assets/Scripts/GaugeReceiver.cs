﻿ using UnityEngine;
using System.Collections.Generic;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class GaugeReceiver : MonoBehaviour
{
    public string bindAddress = "0.0.0.0";          // our own local IP to bind to
    public int port = 2031; //port must be 2031

    public volatile short speed = 0;
    public volatile short rpm = 0;

    private UdpClient udpClient;
    private IPAddress bindIpAddress;
    private Thread receiverThread;
    private IPEndPoint switchEndPoint = new IPEndPoint(IPAddress.Loopback, 5000);
    private bool runReceiver;
    private LinkedList<byte[]> incoming = new LinkedList<byte[]>();

    // Use this for initialization
    void Start()
    {
        bindIpAddress = IPAddress.Parse(bindAddress);

        udpClient = new UdpClient();

        udpClient.ExclusiveAddressUse = false;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        udpClient.Client.Bind(new IPEndPoint(bindIpAddress, port));

        receiverThread = new Thread(Receiver);
        runReceiver = true;
        receiverThread.Start();
    }

    public void OnDestroy()
    {
        Stop();
    }

    public void Stop()
    {
        runReceiver = false;

        if (udpClient != null)
        {
            udpClient.Close();
        }
        if (receiverThread != null)
        {
            receiverThread.Join();
            receiverThread = null;
        }
    }

    private void Receiver()
    {
        Logger.Log(string.Format("[ADL AnalogAbbReceiver] Started - Thread {0}", receiverThread.ManagedThreadId));

        IPEndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

        byte[] sayonara = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        while (runReceiver)
        {
            try
            {
                byte[] buffer = udpClient.Receive(ref remoteEndPoint);

                Debug.Log("Buffer length : " +buffer.Length);

                //Check to see if SimHost is shutting down, if true, sent to switch first before shutting down the dashboard
                if(PLCMaster.ByteArraysEqual(buffer, sayonara))
                {
                    Logger.Log("Signal to quit now!");

                    udpClient.Send(buffer, buffer.Length, switchEndPoint);

                    Application.Quit();

                    break;
                }

                GetDashboardParams(buffer);

                Thread.Sleep(0); // return control to unity sometimes
            }
            catch (SocketException se)
            {
                Debug.Log("Sayonara wasn't caught");

                // ignore error due to remote socket closed.
                Logger.LogError(se.ToString());
            }
            catch (ObjectDisposedException ode)
            {
                Debug.Log("Sayonara 2 wasn't caught");

                Logger.LogError(ode.ToString());
                runReceiver = false;
            }
        }

        Logger.Log(string.Format("[ADL AnalogAbbReceiver] Stopped - Thread {0}", receiverThread.ManagedThreadId));
        receiverThread = null;

    }

    // retrieves values from byte
    private void GetDashboardParams(byte[] data)
    {
        System.IO.BinaryReader reader = new System.IO.BinaryReader(new System.IO.MemoryStream(data));
        //reader.BaseStream.Position = 0;
        //speed = 0;
        //rpm = 0;

        short add = 500;

        speed = reader.ReadInt16();
        rpm = reader.ReadInt16();
        //Debug.Log("Raw RPM Value :" + rpm);
        //rpm += add;
        //Debug.Log("After adding "+add+  " to Raw RPM Value :" + rpm);
        // reverse Network-Host order 0, 125 -> 125, 0
        speed = IPAddress.NetworkToHostOrder(speed);
        rpm = IPAddress.NetworkToHostOrder(rpm);
        //Debug.Log("Raw RPM Value :" + rpm);
        //Logger.Log("Speed after Reverse :" + speed);
        //Logger.Log("RPM after Reverse :" + rpm);

        reader.Close();
    }
}
