﻿using UnityEngine;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

public class MiniLoader : MonoBehaviour {

    private DriveSimConfig config = new DriveSimConfig();
    public DriveSimConfig Config { get { return config; } } 

    private static MiniLoader _instance = null;
    private static System.Object _lock = new System.Object();

    //config file on local
    private const string NODE_NAME_FILE = "nodeName.txt";

    public string NodeName { get; private set; }
    public string VehicleType;
    public int DashboardAnalogRecvPort;
    public int DashboardRecvPort;
    public int DashboardSendToPort;
    public bool DashboardShowHelpText;
    public int DashboardSimPort;
    // this will be the drive sim unity ip address, previously it was abb controller ip address
    public string ioCtrlAddr;
    public int plcPort, plcAckPort;
    private string[] arguments;
    public bool isSwitch = false;
    public int localPort;
    public List<string> argu;

    public static MiniLoader Instance
    {
        get
        {
            lock (_lock)
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MiniLoader>();
                    if (_instance == null)
                    {
                        Debug.LogError("MiniLoader Script must be attached to a GameObject");
                    }
                    DontDestroyOnLoad(_instance);
                }
                return _instance;
            }
        }
    }

    // Use this for initialization
    System.Collections.IEnumerator Start ()
    {
        
//#if UNITY_EDITOR
//        arguments = new string[3] { "", "DASHBOARD", "SWITCH" };
//#else
//        arguments = Environment.GetCommandLineArgs();
//#endif
        arguments = new string[3] { "", "DASHBOARD", "SWITCH" };

        for (int i = 0; i < arguments.Length; i++)
        {
            arguments[i] = arguments[i].ToLower();

            Debug.Log("WE HAVE : " + arguments[i]);

        }

        argu = arguments.ToList<string>();

        argu.RemoveAt(0);

        if (isSwitch == true)
        {
            argu.RemoveAt(0);
        }

        Debug.Log("Argu : " +argu[0]);

        Debug.Log(String.Format("GetCommandLineArgs: {0}", String.Join(", ", arguments)));

        Debug.Log("(╯°□°）╯︵ ┻━┻");

        ////read in config file
        //node name is derived from environment variable. fallback to reading from nodeName.txt if environment variable doesn't exist
        FileInfo nodeNameFile = new FileInfo(NODE_NAME_FILE);
        NodeName = nodeNameFile.Exists ? File.ReadAllText(NODE_NAME_FILE) : System.Environment.GetEnvironmentVariable("DASHBOARD_NAME");

        ////if NodeName is null here, we will fallback to using whatever value is supplied in config
        if (!config.LoadXml(NodeName))
        {
            Logger.LogCritical("[miniLoader] Failed to read external config. Quiting Application");
            Application.Quit();
        }

        Logger.Log(config.ToString());
                
        NodeName = config.NodeName;

        Debug.Log("Node name: " + NodeName);

        ioCtrlAddr = config.DashboardInfo.DashboardIOCtrlAddress;

        Debug.Log("ioCtrlAddr: "+ ioCtrlAddr);

        plcPort = config.DashboardInfo.DashboardRecvPort;

        Debug.Log("plcPort: " + plcPort);

        plcAckPort = config.DashboardInfo.DashboardRecvPort;

        Debug.Log("plcAckPort: " + plcAckPort);

        DashboardAnalogRecvPort = config.DashboardInfo.DashboardAnalogRecvPort;

        Debug.Log("DashboardAnalogRecvPort: " + DashboardAnalogRecvPort);

        DashboardRecvPort = config.DashboardInfo.DashboardRecvPort;

        Debug.Log("DashboardRecvPort: " + DashboardRecvPort);

        DashboardSendToPort = config.DashboardInfo.DashboardSendToPort;

        Debug.Log("DashboardSendToPort: " + DashboardSendToPort);

        DashboardShowHelpText = config.DashboardInfo.DashboardShowHelpText;

        Debug.Log("DashboardShowHelpText: " + DashboardShowHelpText);

        DashboardSimPort = config.DashboardInfo.DashboardSimPort;

        Debug.Log("DashboardSimPort: " + DashboardSimPort);

        VehicleType = config.DashboardInfo.DashboardActualVehType;

        Debug.Log("VehicleType: " + VehicleType);

        //The following if statements will detemine what type of switch and dashboard to load,
        //depending on the config file and also the local environment variable 

        if (VehicleType.Equals("ADL 500"))
        {
            //the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement

            foreach(string argument in argu)
            {
                if (argument.Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");
                    
                    Application.LoadLevelAdditive(1);
                }
                else if (argument.Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");
                    
                    Application.LoadLevelAdditive(2);
                    isSwitch = true;      
                }
           }
        }

        if (VehicleType.Equals("Man A22"))
        {
            //the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement
            foreach (string argument in argu)
            {
                if (argument.Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");
                    
                    Application.LoadLevelAdditive(3);
                }
                else if(argument.Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");
                    
                    Application.LoadLevelAdditive(4);
                    isSwitch = true;                    
                }
            }
        }

        if (VehicleType.Equals("Volvo"))
        {
            //the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement
            foreach (string argument in argu)
            {
                if (argument.Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");
                    
                    Application.LoadLevelAdditive(5);
                }
                else if (argument.Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");
                    
                    Application.LoadLevelAdditive(6);
                    isSwitch = true;                    
                }
            }
        }

        if (VehicleType.Equals("Citaro"))
        {
            //the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement
            foreach (string argument in argu)
            {
                if (argument.Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");
                    
                    Application.LoadLevelAdditive(7);
                }
                else if (argument.Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");
                    
                    Application.LoadLevelAdditive(8);
                    isSwitch = true;
                }
           }
        }

        if (VehicleType.Equals("SCDF Ambulance"))
        {
            ////the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement
            //foreach (string argument in argu)
            //{
                if (argu[0].Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");

                    Application.LoadLevelAdditive(9);
                }
                else if (argu[0].Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");

                    Application.LoadLevelAdditive(10);
                    //isSwitch = true;
                }
            //}
        }

        if (VehicleType.Equals("SCDF Firetruck"))
        {
            //the loop must start from 1 as first element is some other text due to the arguments otherwise it will always jump to else statement
            //foreach (string argument in argu)
            //{
                if (argu[0].Equals("dashboard"))
                {
                    Debug.Log("IT IS A DASHBOADRD");

                    Application.LoadLevelAdditive(11);
                }
                else if (argu[0].Equals("switch"))
                {
                    Debug.Log("IT IS A SWITCH");

                    Application.LoadLevelAdditive(12);
                    isSwitch = true;
                }
            //}
        }

        yield return new WaitForEndOfFrame();
    }
}
