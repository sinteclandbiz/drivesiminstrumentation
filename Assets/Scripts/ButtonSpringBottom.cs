﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(EventTrigger))]
public class ButtonSpringBottom : MonoBehaviour
{
    public ButtonSpringController controller;
    private EventTrigger trigger;
    private Image imageEnable1, imageEnable2, imageDisable;

    // Use this for initialization
    void Start ()
    {
        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 3)
        {
            imageEnable1 = tempImg[0];
            imageEnable2 = tempImg[1];
            imageDisable = tempImg[2];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 3 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();

        EventTrigger.Entry entry1 = new EventTrigger.Entry();
        entry1.eventID = EventTriggerType.PointerDown;
        entry1.callback.AddListener(OnButtonDown);
        trigger.triggers.Add(entry1);

        EventTrigger.Entry entry2 = new EventTrigger.Entry();
        entry2.eventID = EventTriggerType.PointerUp;
        entry2.callback.AddListener(OnButtonUp);
        trigger.triggers.Add(entry2);
    }

    public void OnButtonDown(BaseEventData arg)
    {
        controller.SetBottomTrigger(true,"Bottom");
    }

    public void OnButtonUp(BaseEventData arg)
    {
        controller.SetBottomTrigger(false, "Bottom");
    }

    public void FlipActivatedByOthers()
    {
        imageEnable1.gameObject.SetActive(true);
        imageEnable2.gameObject.SetActive(false);
        imageDisable.gameObject.SetActive(false);
    }

    public void FlipActivatedBySelf()
    {
        imageEnable1.gameObject.SetActive(false);
        imageEnable2.gameObject.SetActive(true);
        imageDisable.gameObject.SetActive(false);
    }

    public void DefaultSettings()
    {
        imageEnable1.gameObject.SetActive(false);
        imageEnable2.gameObject.SetActive(false);
        imageDisable.gameObject.SetActive(true);
    }
}
