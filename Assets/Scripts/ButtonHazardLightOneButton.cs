﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class ButtonHazardLightOneButton : Button
{
    public bool isBatteryOn;
    public int batteryButtonNumber;

    private ulong batteryMask;
    private Image imageEnableWithoutLight, imageEnableWithLight, imageDisable;
    private EventTrigger trigger;
    private bool triggered;
    private bool isOn;
    private IEnumerator sentAfterSomeTimeCoroutine;

    private const int TIMERNUMBER = 1;

    // Use this for initialization
    public new void Start()
    {
        base.Start();

        isOn = false;

        batteryMask = (one << batteryButtonNumber);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 3)
        {
            imageEnableWithoutLight = tempImg[0];
            imageEnableWithLight = tempImg[1];
            imageDisable = tempImg[2];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 6 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);
    }

    public void OnDestroy()
    {
        StopCoroutine(sentAfterSomeTimeCoroutine);

        sentAfterSomeTimeCoroutine = null;
    }


    // Update is called once per frame
    public void Update()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isMatch)
        {
            Logger.Log("Matched! " + gameObject.name + " is flipping now!");

            HandleReceiveFromPLC();
        }

        if (isOn)
        {
            if (isMatch)
            {
                imageEnableWithLight.gameObject.SetActive(true);
                imageEnableWithoutLight.gameObject.SetActive(false);
            }
            else
            {
                imageEnableWithLight.gameObject.SetActive(false);
                imageEnableWithoutLight.gameObject.SetActive(true);
            }
        }
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (triggered)
        {
            //imageEnableWithoutLight.gameObject.SetActive(!imageEnableWithoutLight.gameObject.activeSelf);
            if (isOn)
            {
                imageDisable.gameObject.SetActive(false);
            }
            else
            {
                DefaultSettings();
            }

            triggered = false;
        }
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            if (sentAfterSomeTimeCoroutine == null)
            {
                sentAfterSomeTimeCoroutine = SentAfterSomeTime();
                StartCoroutine(sentAfterSomeTimeCoroutine);

                Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");

                triggered = true;

                isOn = !isOn;
            }
            else
            {
                Debug.Log("Coroutine already is running with something.");
            }
        }
    }

    public void DefaultSettings()
    {
        imageEnableWithoutLight.gameObject.SetActive(false);
        imageEnableWithLight.gameObject.SetActive(false);
        imageDisable.gameObject.SetActive(true);

        //triggered = false;
        isOn = false;
    }

    public IEnumerator SentAfterSomeTime()
    {
        base.SentToPLC();

        yield return new WaitForSeconds(TIMERNUMBER);

        base.SentToPLC();

        StopCoroutine(sentAfterSomeTimeCoroutine);
        sentAfterSomeTimeCoroutine = null;
    }
}
