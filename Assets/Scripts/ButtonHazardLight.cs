﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class ButtonHazardLight : Button
{
    public bool isBatteryOn;
    public int batteryButtonNumber;

    private ulong batteryMask;
    private Image bottomImageEnableWithoutLight, bottomImageEnableWithLight, bottomImageDisable, topImageEnableWithoutLight, topImageEnableWithLight, topImageDisable;
    private EventTrigger trigger;
    private bool triggered;
    private bool isOn;

    //private float timeBeforeChange = 0.5f;
    //public float hazardTimer = 2f;
    //bool isUp = false;
    // Use this for initialization
    public new void Start()
    {
        base.Start();
                
        batteryMask = (one << batteryButtonNumber);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 6)
        {
            bottomImageEnableWithoutLight = tempImg[0];
            bottomImageEnableWithLight = tempImg[1];
            bottomImageDisable = tempImg[2];
            topImageEnableWithoutLight = tempImg[3];
            topImageEnableWithLight = tempImg[4];
            topImageDisable = tempImg[5];
            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 6 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);
    }


    // Update is called once per frame
    public void Update ()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isMatch)
        {
            //Logger.Log("Matched! " + gameObject.name + " is flipping now!");

            HandleReceiveFromPLC();
        }

        if (isOn)
        {
            if (isMatch)
            {
                bottomImageEnableWithLight.gameObject.SetActive(true);
                topImageEnableWithLight.gameObject.SetActive(true);
            }
            else
            {
                bottomImageEnableWithLight.gameObject.SetActive(false);
                topImageEnableWithLight.gameObject.SetActive(false);
            }
        }
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (triggered)
        {
            bottomImageEnableWithoutLight.gameObject.SetActive(!bottomImageEnableWithoutLight.gameObject.activeSelf);

            bottomImageDisable.gameObject.SetActive(!bottomImageDisable.gameObject.activeSelf);

            topImageEnableWithoutLight.gameObject.SetActive(!topImageEnableWithoutLight.gameObject.activeSelf);

            topImageDisable.gameObject.SetActive(!topImageDisable.gameObject.activeSelf);

            triggered = false;
        }
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");

            triggered = true;

            isOn = !isOn;

            base.SentToPLC();          
        }
    }

    public void DefaultSettings()
    {
        bottomImageEnableWithoutLight.gameObject.SetActive(false);
        bottomImageEnableWithLight.gameObject.SetActive(false);
        bottomImageDisable.gameObject.SetActive(true);

        topImageEnableWithoutLight.gameObject.SetActive(false);
        topImageEnableWithLight.gameObject.SetActive(false);
        topImageDisable.gameObject.SetActive(true);
        triggered = false;
        isOn = false;
    }
}
