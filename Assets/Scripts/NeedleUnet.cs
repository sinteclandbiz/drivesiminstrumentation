﻿using UnityEngine;
/// <summary>
/// base class for speedometer and RPM
/// </summary>
public class NeedleUnet : MonoBehaviour
{
    public float minNumber, maxNumber;
    public float targetNumber;
    public float minimumRotation;
    public float maximumRotation;

    private float regulatedNumber; // contains the tabulation of the actual rotation result
    private RectTransform needle;
    //private GameObject gaugeReceiver;

    //protected GaugeReceiver gauge;
    //protected DashboardVarObj dashboardVarObj;

    float percentageOfTargetValueToMax = 0;
    // Use this for initialization
    public virtual void Start ()
    {
        // change this
        //gaugeReceiver = GameObject.Find("/GaugeReciever");

        needle = gameObject.GetComponent<RectTransform>();

        //dashboardVarObj = NetworkClientController.instance.uNetVarObj.dashboardVarObj;

        regulatedNumber = 0f;
    }
	
	// Update is called once per frame
	public virtual void Update ()
    {
        //Debug.Log("PERCENTAGE : " + targetNumber / maxNumber);

        SetValue();

        //regulatedNumber = CalculateRotation(targetNumber);
        //Debug.Log("Regulated number : "+regulatedNumber);
        //needle.localRotation = Quaternion.Euler(new Vector3(0f, 0f, regulatedNumber));

        // outputs the rotation by lerp value
        regulatedNumber = Mathf.Lerp(minimumRotation, maximumRotation, targetNumber / maxNumber);

        needle.localRotation = Quaternion.Euler(new Vector3(0f, 0f, regulatedNumber));
    }

    private float CalculateRotation(float number)
    {
        return minimumRotation - ((number - minNumber) / (maxNumber - minNumber)) * maximumRotation;
    } 

    protected virtual void SetValue()
    {
        // contains the tabulated difference of the actual speed/rpm send by simhost/plc
    }
}
