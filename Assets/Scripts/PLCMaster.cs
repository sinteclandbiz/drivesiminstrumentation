﻿/**
 * ABB Receiver - receives UDP packet from AAB controller and acknowledges them
 * @author Tan Hock Woo <tanhw@stee.stengg.com>


 This class is no longer ABB Receiver, this is a class that can mutate according to whether is it dashboard or switch

 if this is a dashboard, take note that you have to receive from PLC AND sent whatever received to switch!

 if this is a switch, you just need to listen from Dashboard and sent whatever buttons pressed to PLC!
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UnityEngine;

public class PLCMaster
{
    //private static readonly byte[] RESET_PLC_VALUE = new byte[] { 0xFF };

    public string bindAddress = "0.0.0.0";			// our own local IP to bind to
    public int port = 2030;
    public int ackPort = 2030;

    public bool shouldSendAck = true;
    [HideInInspector]
    public ulong switchesState;

    private IPEndPoint remoteEndPoint;
    private IPEndPoint switchEP = new IPEndPoint(IPAddress.Loopback, 5000);
    private volatile UdpClient udpClient;
    private IPAddress bindIpAddress;
    private Thread receiverThread;
    private volatile bool runReceiver;
    private volatile LinkedList<byte[]> incoming = new LinkedList<byte[]>();

    public string PLCAddress = "";
    [HideInInspector]
    public bool isResetComplete = false;
    public const byte RESETCOMMAND = 255;
    private UdpClient udpClientSender;

    //method to convert ulong values into string
    public static string ToBin(ulong value, int len)
    {
        return (len > 1 ? ToBin(value >> 1, len - 1) : null) + "01"[(int)(value & 1)];
    }

    public PLCMaster(string plcAdd, int plcPort, int plcAckPort)
    {
        this.PLCAddress = plcAdd;

        this.port = plcPort;
        this.ackPort = plcAckPort;

        // set up to connect to plc 
        udpClientSender = new UdpClient();
        udpClientSender.ExclusiveAddressUse = false;
        udpClientSender.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        Logger.Log("Before Connect");

        if (MiniLoader.Instance.isSwitch)
        {
            shouldSendAck = false;
        }
        else
        {
            udpClientSender.Connect(new IPEndPoint(IPAddress.Parse(PLCAddress), port));
        }

        Logger.Log("After Connect");

        bindIpAddress = IPAddress.Parse(bindAddress);

        remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

        // setup to receive on your local machine
        // this is the part if you are a dashboard, you will be connected to PLC's IP and port set to 3060
        //if you are a switch, you will connect to ownself and listen from dashboard and port will be set to 5000
        udpClient = new UdpClient();
        udpClient.ExclusiveAddressUse = false;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        udpClient.Client.Bind(new IPEndPoint(bindIpAddress, port));

        //start new thread to listen and sent commands to and fro plc
        runReceiver = true;
        receiverThread = new Thread(Receiver);
        receiverThread.IsBackground = true;
        receiverThread.Start();
    }

    public void OnDestroy()
    {
        Stop();
    }

    /// <summary>
    /// Constantly decode whatever PLC sends to the local PC
    /// if you are switch, you listen from dashboard and decodes whatever was send
    /// </summary>
    public void Update()
    {
        // run when we are drawing 
        byte[] packet = null;

        while (Read(ref packet))
        {
            // value recieved from PLC is converted and stored in this variable!
            switchesState = GetSwitchesState(packet);

            Logger.Log(string.Format("[AbbReceiver] switches: {0}", switchesState.ToString("X")));

            Logger.Log("Received from PLC : " + ToBin(switchesState, 48));
        }
    }

    public void clearSwitchesState()
    {
        switchesState = 0;
    }

    /// <summary>
    /// cleanup when program is closed
    /// </summary>
    public void Stop()
    {
        runReceiver = false;

        if (udpClient != null)
        {
            Logger.LogInfo(string.Format("Shutting down AbbReceiver UDPClient bound to {0}:{1}", bindIpAddress, port));
            udpClient.Close();
            Logger.LogInfo(string.Format("Successfully shut down AbbReceiver UDPClient bound to {0}:{1}", bindIpAddress, port));
        }
        if (receiverThread != null && receiverThread.IsAlive)
        {
            Logger.LogInfo("Receiver thread in AbbReceiver is still alive");
            receiverThread.Interrupt();
            runReceiver = false;
            receiverThread.Join(500);
            Logger.LogInfo("Receiver thread in AbbReceiver has joined");
        }

        if (udpClientSender != null)
        {
            Logger.LogInfo(string.Format("Shutting down AbbReceiver UDPClientSender connected to {0}:{1}", PLCAddress, port));
            udpClientSender.Close();
            Logger.LogInfo(string.Format("Successfully shut down AbbReceiver UDPClientSender connected to {0}:{1}", PLCAddress, port));
        }
    }

    /// <summary>
    /// read the packets method
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    public bool Read(ref byte[] data)
    {
        lock (incoming)
        {

            if (incoming.Count < 1)
            {
                return false;
            }
            data = incoming.First.Value;
            incoming.RemoveFirst();
            return true;
        }
    }

    public static bool ByteArraysEqual(byte[] b1, byte[] b2)
    {
        if (b1 == b2) return true;
        if (b1 == null || b2 == null) return false;
        if (b1.Length != b2.Length) return false;
        for (int i = 0; i < b1.Length; i++)
        {
            if (b1[i] != b2[i]) return false;
        }
        return true;
    }

    /// <summary>
    /// Thread constantly gets data from PLC / dashboard
    /// </summary>
    private void Receiver()
    {
        Logger.Log(string.Format("[AbbReceiver] Started - Thread {0}", receiverThread.ManagedThreadId));

        byte[] sayonara = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        while (runReceiver)
        {           
            try
            {
                byte[] buffer = udpClient.Receive(ref remoteEndPoint);

                Debug.Log("Buffer length : " + buffer.Length);

                if (MiniLoader.Instance.isSwitch == false)
                {
                    udpClient.Send(buffer, buffer.Length, switchEP);
                }

                if(MiniLoader.Instance.isSwitch)
                {
                    //if(buffer.Length > 6)
                    if (ByteArraysEqual(buffer, sayonara))
                    {
                        Logger.Log("Signal to quit now!");

                        Application.Quit();

                        break;
                    }
                }

                // Receive data packet from IO Controller 
                lock (incoming)
                {
                    incoming.AddLast(buffer);

                    Logger.Log("Receive " + BinToString(buffer) + " from PLC.");

                    if (shouldSendAck)
                    {
                        if (ackPort != 0) { remoteEndPoint.Port = ackPort; }

                        SendData(buffer);
                    }
                }
            }
            catch (SocketException se)
            {
                // ignore error due to remote socket closed.
                Logger.LogError(se.ToString());
            }
            catch (ObjectDisposedException ode)
            {
                Logger.LogError(ode.ToString());
                runReceiver = false;
            }
            catch (Exception e)
            {
                Logger.LogError(e.ToString());
            }
        }

        Logger.Log(string.Format("[AbbReceiver] Stopped - Thread {0}", receiverThread.ManagedThreadId));
    }

    public void SendData(byte[] data)
    {
        try
        {
            udpClientSender.Send(data, data.Length);
            Logger.Log(string.Format("[AbbReceiver] ACK Sent {0}", BinToString(data)));
        }
        catch (SocketException se)
        {
            // ignore error due to remote socket closed.
            Logger.LogError(se.ToString());
        }
        catch (ObjectDisposedException ode)
        {
            Logger.LogError(ode.ToString());
        }
    }

    public string BinToString(byte[] data)
    {
        string result = "";

        foreach (byte b in data)
        {
            int mask = 0x01;
            for (int n = 0; n < 8; ++n)
            {
                result = (((b & mask) == mask) ? "1" : "0") + result;
                mask <<= 1;
            }
        }

        return result;
    }

    private string HexStr(byte[] data)
    {
        string result = "";
        foreach (byte b in data)
        {
            result += string.Format("{0:X}", b);
        }
        return result;
    }

    /// <summary>
    /// Decodes the byte packet sent by PLC
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private ulong GetSwitchesState2(byte[] data)
    {
        MemoryStream stream = new MemoryStream(data);
        BinaryReader reader = new BinaryReader(stream);

        ulong result = 0;
        long value = 0;

        if (data.Length >= sizeof(Int64))
        {
            value = reader.ReadInt64();
        }
        else if (data.Length >= sizeof(Int32))
        {
            value = (long)reader.ReadInt32();
        }
        else if (data.Length >= sizeof(Int16))
        {
            value = (long)reader.ReadInt16();
        }
        else if (data.Length >= sizeof(byte))
        {
            value = (long)reader.ReadByte();
        }
        reader.Close();

        result = (ulong)value;

        return result;
    }

    /// <summary>
    /// Decodes the byte packet sent by PLC, will only work for up to 64 bits (sizeof(ulong))
    /// </summary>
    /// <param name="data"></param>
    /// <returns></returns>
    private ulong GetSwitchesState(byte[] data)
    {
        ulong result = 0;
        //        for (int n = data.Length-1; n >= 0;  --n)
        for (int n = 0; n < data.Length; ++n)
        {
            result <<= 8;
            result |= data[n];
        }
        return result;
    }
}
