﻿using UnityEngine;
using System;

public class test : MonoBehaviour {

	// Use this for initialization
	void Start () {

        ulong one = 1;
        ulong topToggleNumber = 2;
        ulong topToggleNumber2 = 4;
        ulong topToggleNumber3 = 17;
        int number = 17;
        ulong topToggleNumber4 = topToggleNumber3 ^ topToggleNumber3;
        ulong mask = (one << number);

        byte[] byteArray;
        byte[] byteArray2;

        byteArray = BitConverter.GetBytes(topToggleNumber);
        byteArray2 = BitConverter.GetBytes(topToggleNumber2);
        byte[] byteArray3 = BitConverter.GetBytes(topToggleNumber3);
        byte[] byteArray4 = BitConverter.GetBytes(topToggleNumber4);
        byte[] byteArray5 = BitConverter.GetBytes(mask);

        //byte[] byteArray3 = new byte[byteArray.Length + byteArray2.Length];

        //System.Buffer.BlockCopy(byteArray, 0, byteArray3, 0, byteArray.Length);
        //System.Buffer.BlockCopy(byteArray2, 0, byteArray3, 0, byteArray2.Length);

        byte[] result = new byte[byteArray.Length];

        for(int i = 0; i < byteArray.Length; i++)
        {
            result[i] = (byte) (byteArray[i] ^ byteArray2[i]);
        }

        Debug.Log(BinToString(byteArray));
        Debug.Log(BinToString(byteArray2));
        Debug.Log(BinToString(byteArray3));
        Debug.Log(BinToString(result));
        Debug.Log(BinToString(byteArray4));
        Debug.Log(BinToString(byteArray5));

    }

    // Update is called once per frame
    void Update () {
	
	}

    public string BinToString(byte[] data)
    {
        string result = "";

        foreach (byte b in data)
        {
            int mask = 0x01;
            for (int n = 0; n < 8; ++n)
            {
                result = (((b & mask) == mask) ? "1" : "0") + result;
                mask <<= 1;
            }
        }

        return result;
    }

}
