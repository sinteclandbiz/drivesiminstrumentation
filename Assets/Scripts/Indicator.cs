﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class Indicator : MonoBehaviour
{
    private Image image;
    public int bitPosition;

    // Use this for initialization
    void Start()
    {
        if (image == null)
        {
            image = GetComponent<Image>();
            
        }
        else
        {
            Debug.Log("Where's my image file for Gameobject <" + gameObject.name + ">?!?!");
        }
    }

    public void Update()
    {
        ulong one = 1;
        ulong mask = ( one << bitPosition);

        //Debug.Log("Mask value is " +mask);

        image.enabled = (InstrumentController.Instance.plcMaster.switchesState & mask) == mask;

        //Debug.Log(InstrumentController.Instance.abbReceiver.switchesState & mask);

        //ulong mask = (ulong) (Math.Pow(2, 47)) >> bitPosition;   

        //image.enabled = (InstrumentController.Instance.abbReceiver.switchesState & mask) == mask;

        //Debug.Log("Mask : " +mask);
    }
}
