﻿using UnityEngine;
using TMPro;

public class TextMeshProSpeedometerUnet : MonoBehaviour
{
    private TextMeshProUGUI textMeshPro;

    private void Start()
    {
        textMeshPro = this.GetComponent<TextMeshProUGUI>();
    }

    private void Update()
    {
        textMeshPro.text = NetworkClientController.instance.uNetVarObj.dashboardVarObj.speed.ToString("f0");
    }
}
