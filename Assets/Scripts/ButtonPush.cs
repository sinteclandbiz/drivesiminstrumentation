﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class ButtonPush : Button
{
    public bool isBatteryOn;
    public int batteryButtonNumber;

    private Image imageDown, imageUp;
    private EventTrigger trigger;
    private ulong batteryMask;
    private bool triggered;
    private bool isOff;

    public new void Start()
    {
        triggered = false;

        isOff = false;

        base.Start();

        batteryMask = (one << batteryButtonNumber);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 2)
        {
            imageDown = tempImg[0];

            imageUp = tempImg[1];

            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 2 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);
    }

    public void Update()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false)
        {
            //isOff = true;
            DefaultSettings();
        }

        if (triggered)
        {
            HandleReceiveFromPLC();
        }
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");

            StartCoroutine("SentAfterOneSec");

            triggered = true;
        }
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (triggered)
        {
            imageDown.gameObject.SetActive(!imageDown.gameObject.activeSelf);
            imageUp.gameObject.SetActive(!imageUp.gameObject.activeSelf);
            triggered = false;        
        }
    }

    public IEnumerator SentAfterOneSec()
    {
        base.SentToPLC();

        yield return new WaitForSeconds(1);

        base.SentToPLC();
    }

    public void DefaultSettings()
    {
        imageDown.gameObject.SetActive(false);
        imageUp.gameObject.SetActive(true);
        triggered = false;
    }
}
