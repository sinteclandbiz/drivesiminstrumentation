﻿using UnityEngine;
using System;
using UnityEngine.EventSystems;

//base class of a button
public class Button : MonoBehaviour
{
    public int thisButtonNumber;
    public int expectedReplyNumber;

    //maybe implemented wrongly, comment for now
    //public delegate void ReceiveFromPLCHandler();
    //public event ReceiveFromPLCHandler ReceivedFromPLCHandler;

    protected ulong one = 1;
    protected byte[] byteArray {get; set; }
    protected byte[] byteArray2 { get; set; }

    protected ulong mask { get; set; }
    protected ulong mask2 { get; set; }
    protected bool isMatch { get; set; }

    // Use this for initialization
    public void Start ()
    {
        mask = (one << thisButtonNumber);

        byteArray = BitConverter.GetBytes(mask);

        mask2 = (one << expectedReplyNumber);

        byteArray2 = BitConverter.GetBytes(mask2);
    }

    protected void InitAllBitMask()
    {
        mask = (one << thisButtonNumber);

        byteArray = BitConverter.GetBytes(mask);

        mask2 = (one << expectedReplyNumber);

        byteArray2 = BitConverter.GetBytes(mask2);
    }

    protected virtual void HandleReceiveFromPLC()
    {
        Logger.Log("You should not see this message! This is from parent Button!");
    }

    public void SentToPLC()
    {
        Debug.Log(gameObject.name + " is sending to PLC the following bytes: " +InstrumentController.Instance.plcMaster.BinToString(byteArray));
        InstrumentController.Instance.buttonController.MergeForSending(byteArray);
    }

    //maybe implemented wrongly, comment for now
    //protected void OnReceivedFromPLC()
    //{
    //    var handler = ReceivedFromPLCHandler;
    //    if (handler != null)
    //    {
    //        handler();
    //    }
    //}

    public virtual void OnClickSent(BaseEventData arg)
    {

    }
}
