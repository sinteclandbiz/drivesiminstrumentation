﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections;

[RequireComponent(typeof(EventTrigger))]
public class ButtonPulse : Button
{
    public bool isBatteryOn;
    public int batteryButtonNumber;
    public int neutralGearNumber;
    public ButtonGear neutral;
    public TextHelpListenFromButton txtHelp;
    public bool sentNeutralBytesToo;

    //interval for countdown
    private const int TIMERNUMBER = 5;

    private Image bottomImageEnable, bottomImageDisable, topImageEnable, topImageDisable;
    private EventTrigger trigger;
    private ulong batteryMask;
    private bool triggered;
    private bool isOff;
    private ulong neutralGearMask;
    private byte[] neutralGearByte;

    public new void Start()
    {
        base.Start();

        triggered = false;

        isOff = false;
               
        batteryMask = (one << batteryButtonNumber);

        neutralGearMask = (one << neutralGearNumber);

        neutralGearByte = BitConverter.GetBytes(neutralGearMask);

        Image[] tempImg = GetComponentsInChildren<Image>();

        if (tempImg.Length == 4)
        {
            bottomImageEnable = tempImg[0];
            bottomImageDisable = tempImg[1];
            topImageEnable = tempImg[2];
            topImageDisable = tempImg[3];
            DefaultSettings();
        }
        else
        {
            Logger.LogCritical("Need 2 images laaaaaaaaaaaaaaaaaaaaa but you have " + tempImg.Length);
        }

        trigger = GetComponentInParent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener(OnClickSent);
        trigger.triggers.Add(entry);
    }

    public void Update()
    {
        isBatteryOn = (InstrumentController.Instance.plcMaster.switchesState & batteryMask) == batteryMask;

        isMatch = (InstrumentController.Instance.plcMaster.switchesState & mask2) == mask2;

        if (isBatteryOn == false)
        {
            DefaultSettings();
        }

        if (triggered)
        {
            HandleReceiveFromPLC();
        }
    }

    public override void OnClickSent(BaseEventData arg)
    {
        if (isBatteryOn == false)
        {
            Logger.Log("Battery is not on, please check if it's on laaaa.");
        }
        else
        {
            Logger.Log("Successfully registered click " + gameObject.name + ", now sending!");

            StartCoroutine("SentAfterSomeTime");

            triggered = true;

            if(txtHelp != null)
            {
                txtHelp.DisplayOff();
            }
        }

        if (sentNeutralBytesToo)
        {
            neutral.isOn = true;

            InstrumentController.Instance.buttonController.MergeForSending(neutralGearByte);

            StartCoroutine("SentAfterSomeTime");
        }
    }

    protected override void HandleReceiveFromPLC()
    {
        //Debug.Log(gameObject.name + "'s HandleReceiveFromPLC is triggered.");
        if (triggered)
        {
            bottomImageEnable.gameObject.SetActive(!bottomImageEnable.gameObject.activeSelf);
            bottomImageDisable.gameObject.SetActive(!bottomImageDisable.gameObject.activeSelf);
            topImageEnable.gameObject.SetActive(!topImageEnable.gameObject.activeSelf);
            topImageDisable.gameObject.SetActive(!topImageDisable.gameObject.activeSelf);
            
            triggered = false;
        }
    }

    public IEnumerator SentAfterSomeTime()
    {
        base.SentToPLC();

        yield return new WaitForSeconds(TIMERNUMBER);

        base.SentToPLC();
    }

    public void DefaultSettings()
    {
        bottomImageEnable.gameObject.SetActive(false);
        bottomImageDisable.gameObject.SetActive(true);
        topImageEnable.gameObject.SetActive(false);
        topImageDisable.gameObject.SetActive(true);
        triggered = false;
        if(neutral != null)
        {
            neutral.isOn = false;
        }
        if (txtHelp != null)
        {
            txtHelp.DefaultSettings();
        }
    }
}
