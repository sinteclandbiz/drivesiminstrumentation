﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class FixUNETSceneIds : MonoBehaviour
{
    [PostProcessScene(10)]
    public static void OnPostProcessScene()
    {
        Debug.Log("LOADING THIS ON PROCESS SCENE");
        int nextSceneId = 1;

        //Is the list ordered the same way always or am I insane?
        List<NetworkIdentity> list = Resources.FindObjectsOfTypeAll<NetworkIdentity>().ToList<NetworkIdentity>();
        list.RemoveAll(go => PrefabUtility.GetPrefabParent(go) == null && PrefabUtility.GetPrefabObject(go) != null);
        list = list.OrderBy(x => x.gameObject.name).ToList<NetworkIdentity>();

        foreach (NetworkIdentity uv in list)
        {
            if (uv.isClient || uv.isServer)
                continue;

            //Override original UNET code
            //uv.gameObject.SetActive(false); -- no need, already disabled
            uv.ForceSceneId(nextSceneId++);
        }

    }

    //// called zero
    //void Awake()
    //{
    //    Debug.Log("Awake");
    //}

    //// called first
    //void OnEnable()
    //{
    //    Debug.Log("OnEnable called");
    //    SceneManager.sceneLoaded += OnSceneLoaded;

    //    int nextSceneId = 1;

    //    //Is the list ordered the same way always or am I insane?
    //    List<NetworkIdentity> list = Resources.FindObjectsOfTypeAll<NetworkIdentity>().ToList<NetworkIdentity>();
    //    //list.RemoveAll(go => PrefabUtility.GetPrefabParent(go) == null && PrefabUtility.GetPrefabObject(go) != null);
    //    list = list.OrderBy(x => x.gameObject.name).ToList<NetworkIdentity>();

    //    foreach (NetworkIdentity uv in list)
    //    {
    //        if (uv.isClient || uv.isServer)
    //            continue;

    //        //Override original UNET code
    //        //uv.gameObject.SetActive(false); -- no need, already disabled
    //        uv.ForceSceneId(nextSceneId++);
    //    }
    //}

    //// called second
    //void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    //{
    //    Debug.Log("OnSceneLoaded: " + scene.name);
    //    Debug.Log(mode);

    //    int nextSceneId = 1;

    //    //Is the list ordered the same way always or am I insane?
    //    List<NetworkIdentity> list = Resources.FindObjectsOfTypeAll<NetworkIdentity>().ToList<NetworkIdentity>();
    //    //list.RemoveAll(go => PrefabUtility.GetPrefabParent(go) == null && PrefabUtility.GetPrefabObject(go) != null);
    //    list = list.OrderBy(x => x.gameObject.name).ToList<NetworkIdentity>();

    //    foreach (NetworkIdentity uv in list)
    //    {
    //        if (uv.isClient || uv.isServer)
    //            continue;

    //        //Override original UNET code
    //        //uv.gameObject.SetActive(false); -- no need, already disabled
    //        uv.ForceSceneId(nextSceneId++);
    //    }
    //}

    //// called third
    //void Start()
    //{
    //    Debug.Log("Start");

    //    int nextSceneId = 1;

    //    //Is the list ordered the same way always or am I insane?
    //    List<NetworkIdentity> list = Resources.FindObjectsOfTypeAll<NetworkIdentity>().ToList<NetworkIdentity>();
    //    //list.RemoveAll(go => PrefabUtility.GetPrefabParent(go) == null && PrefabUtility.GetPrefabObject(go) != null);
    //    list = list.OrderBy(x => x.gameObject.name).ToList<NetworkIdentity>();

    //    foreach (NetworkIdentity uv in list)
    //    {
    //        if (uv.isClient || uv.isServer)
    //            continue;

    //        //Override original UNET code
    //        //uv.gameObject.SetActive(false); -- no need, already disabled
    //        uv.ForceSceneId(nextSceneId++);
    //    }
    //}

    //// called when the game is terminated
    //void OnDisable()
    //{
    //    Debug.Log("OnDisable");
    //    SceneManager.sceneLoaded -= OnSceneLoaded;
    //}
}