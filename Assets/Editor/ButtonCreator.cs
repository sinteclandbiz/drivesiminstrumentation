﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class ButtonToggleCreator : MonoBehaviour {

    [MenuItem("MyTools/Create Toggle Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if(Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?","Select something to parent laaaaaaaaaaa.","Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "buttonControl" };

        //create the gameobject and assigned its name immediately
        GameObject bottomImageEnabled = new GameObject() { name = "bottomImageEnabled" };
        GameObject bottomImageDisabled = new GameObject() { name = "bottomImageDisabled" };
        GameObject topImageEnabled = new GameObject() { name = "topImageEnabled" };
        GameObject topImageDisabled = new GameObject() { name = "topImageDisabled" };

        //add the component Image to make it Image
        bottomImageEnabled.AddComponent<Image>();
        bottomImageDisabled.AddComponent<Image>();
        topImageEnabled.AddComponent<Image>();
        topImageDisabled.AddComponent<Image>();

        //parent it to buttonControl
        bottomImageEnabled.transform.SetParent(buttonControl.transform);
        bottomImageDisabled.transform.SetParent(buttonControl.transform);
        topImageEnabled.transform.SetParent(buttonControl.transform);
        topImageDisabled.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonToggle>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}


public class ButtonBatteryToggleCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Battery Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "batteryButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject bottomImageEnabled = new GameObject() { name = "bottomImageEnabled" };
        GameObject bottomImageDisabled = new GameObject() { name = "bottomImageDisabled" };
        GameObject topImageEnabled = new GameObject() { name = "topImageEnabled" };
        GameObject topImageDisabled = new GameObject() { name = "topImageDisabled" };

        //add the component Image to make it Image
        bottomImageEnabled.AddComponent<Image>();
        bottomImageDisabled.AddComponent<Image>();
        topImageEnabled.AddComponent<Image>();
        topImageDisabled.AddComponent<Image>();

        //parent it to buttonControl
        bottomImageEnabled.transform.SetParent(buttonControl.transform);
        bottomImageDisabled.transform.SetParent(buttonControl.transform);
        topImageEnabled.transform.SetParent(buttonControl.transform);
        topImageDisabled.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonBatteryToggle>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonRotaryToggleCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Rotary Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "rotaryButton" };

        buttonControl.AddComponent<Image>();

        //attach the button logic
        buttonControl.AddComponent<ButtonRotaryToggle>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonPushCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Push Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "pushButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject imageDown = new GameObject() { name = "imageDown" };
        GameObject imageUp = new GameObject() { name = "imageUp" };

        //add the component Image to make it Image
        imageDown.AddComponent<Image>();
        imageUp.AddComponent<Image>();

        //parent it to buttonControl
        imageDown.transform.SetParent(buttonControl.transform);
        imageUp.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonPush>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonPulseCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Pulse Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "pulseButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject bottomImageEnabled = new GameObject() { name = "bottomImageEnabled" };
        GameObject bottomImageDisabled = new GameObject() { name = "bottomImageDisabled" };
        GameObject topImageEnabled = new GameObject() { name = "topImageEnabled" };
        GameObject topImageDisabled = new GameObject() { name = "topImageDisabled" };

        //add the component Image to make it Image
        bottomImageEnabled.AddComponent<Image>();
        bottomImageDisabled.AddComponent<Image>();
        topImageEnabled.AddComponent<Image>();
        topImageDisabled.AddComponent<Image>();

        //parent it to buttonControl
        bottomImageEnabled.transform.SetParent(buttonControl.transform);
        bottomImageDisabled.transform.SetParent(buttonControl.transform);
        topImageEnabled.transform.SetParent(buttonControl.transform);
        topImageDisabled.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonPulse>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonGearCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Gear Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "gearButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject imageDown = new GameObject() { name = "imageDown" };
        GameObject imageUp = new GameObject() { name = "imageUp" };

        //add the component Image to make it Image
        imageDown.AddComponent<Image>();
        imageUp.AddComponent<Image>();

        //parent it to buttonControl
        imageDown.transform.SetParent(buttonControl.transform);
        imageUp.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonGear>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonHazardLightCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Hazard Light Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "hazardButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject bottomImageEnableWithoutLight = new GameObject() { name = "bottomImageEnableWithoutLight" };
        GameObject bottomImageEnableWithLight = new GameObject() { name = "bottomImageEnableWithLight" };
        GameObject bottomImageDisable = new GameObject() { name = "bottomImageDisable" };
        GameObject topImageEnableWithoutLight = new GameObject() { name = "topImageEnableWithoutLight" };
        GameObject topImageEnableWithLight = new GameObject() { name = "topImageEnableWithLight" };
        GameObject topImageDisable = new GameObject() { name = "topImageDisable" };

        //add the component Image to make it Image
        bottomImageEnableWithoutLight.AddComponent<Image>();
        bottomImageEnableWithLight.AddComponent<Image>();
        bottomImageDisable.AddComponent<Image>();
        topImageEnableWithoutLight.AddComponent<Image>();
        topImageEnableWithLight.AddComponent<Image>();
        topImageDisable.AddComponent<Image>();

        //parent it to buttonControl
        bottomImageEnableWithoutLight.transform.SetParent(buttonControl.transform);
        bottomImageEnableWithLight.transform.SetParent(buttonControl.transform);
        bottomImageDisable.transform.SetParent(buttonControl.transform);
        topImageEnableWithoutLight.transform.SetParent(buttonControl.transform);
        topImageEnableWithLight.transform.SetParent(buttonControl.transform);
        topImageDisable.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonHazardLight>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonOneHazardLightCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Hazard Light One Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "hazardButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject imageEnableWithoutLight = new GameObject() { name = "imageEnableWithoutLight" };
        GameObject imageEnableWithLight = new GameObject() { name = "imageEnableWithLight" };
        GameObject imageDisable = new GameObject() { name = "imageDisable" };

        //add the component Image to make it Image
        imageEnableWithoutLight.AddComponent<Image>();
        imageEnableWithLight.AddComponent<Image>();
        imageDisable.AddComponent<Image>();


        //parent it to buttonControl
        imageEnableWithoutLight.transform.SetParent(buttonControl.transform);
        imageEnableWithLight.transform.SetParent(buttonControl.transform);
        imageDisable.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonHazardLightOneButton>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}

public class ButtonDoubleSidedRockerCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Double Sided Rocker Controls")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonController = new GameObject()
        { name = "DOubleSidedRockerButtonController" };

        GameObject topButtonControl = new GameObject()
        { name = "topButtonControl" };

        GameObject bottomButtonControl = new GameObject()
        { name = "bottomButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject topImageEnable1 = new GameObject() { name = "topImageEnable1" };
        GameObject topImageEnable2 = new GameObject() { name = "topImageEnable2" };
        GameObject topImageDisable = new GameObject() { name = "imageDisable" };

        GameObject bottomImageEnable1 = new GameObject() { name = "bottomImageEnable1" };
        GameObject bottomImageEnable2 = new GameObject() { name = "bottomImageEnable2" };
        GameObject bottomImageDisable = new GameObject() { name = "bottomImageDisable" };

        //add the component Image to make it Image
        topImageEnable1.AddComponent<Image>();
        topImageEnable2.AddComponent<Image>();
        topImageDisable.AddComponent<Image>();

        bottomImageEnable1.AddComponent<Image>();
        bottomImageEnable2.AddComponent<Image>();
        bottomImageDisable.AddComponent<Image>();

        //parent it to buttonControl
        topImageEnable1.transform.SetParent(topButtonControl.transform);
        topImageEnable2.transform.SetParent(topButtonControl.transform);
        topImageDisable.transform.SetParent(topButtonControl.transform);

        bottomImageEnable1.transform.SetParent(bottomButtonControl.transform);
        bottomImageEnable2.transform.SetParent(bottomButtonControl.transform);
        bottomImageDisable.transform.SetParent(bottomButtonControl.transform);

        topButtonControl.transform.SetParent(buttonController.transform);
        bottomButtonControl.transform.SetParent(buttonController.transform);

        //attach the button logic for both top and bottom button
        buttonController.AddComponent<ButtonSpringController>();
        topButtonControl.AddComponent<ButtonSpringTop>();
        bottomButtonControl.AddComponent<ButtonSpringBottom>();
        //set the buttonControl as child object under the selected gameobject
        buttonController.transform.SetParent(Selection.activeTransform);

        buttonController.transform.localScale = Vector3.one;
    }
}

public class SingleSidedRockerButtonCreator : MonoBehaviour
{
    [MenuItem("MyTools/Create Single Sided Rocker Button")]
    static void Create()
    {
        //Selection is for clicking on the gameobject and doing something to it,
        //in this case it is for selecting a gameobject for us to create the relevant
        //buttons
        if (Selection.activeObject == null)
        {
            //pops up a dialog box when nothing is selected
            EditorUtility.DisplayDialog("Why you never select anything?", "Select something to parent laaaaaaaaaaa.", "Select something ok?");
            return;
        }

        //creating the variables for the 'button'
        GameObject buttonControl = new GameObject()
        { name = "singleSidedRockerButtonControl" };

        //create the gameobject and assigned its name immediately
        GameObject bottomImageEnabled = new GameObject() { name = "bottomImageEnabled" };
        GameObject bottomImageDisabled = new GameObject() { name = "bottomImageDisabled" };
        GameObject topImageEnabled = new GameObject() { name = "topImageEnabled" };
        GameObject topImageDisabled = new GameObject() { name = "topImageDisabled" };

        //add the component Image to make it Image
        bottomImageEnabled.AddComponent<Image>();
        bottomImageDisabled.AddComponent<Image>();
        topImageEnabled.AddComponent<Image>();
        topImageDisabled.AddComponent<Image>();

        //parent it to buttonControl
        bottomImageEnabled.transform.SetParent(buttonControl.transform);
        bottomImageDisabled.transform.SetParent(buttonControl.transform);
        topImageEnabled.transform.SetParent(buttonControl.transform);
        topImageDisabled.transform.SetParent(buttonControl.transform);

        //attach the button logic
        buttonControl.AddComponent<ButtonSpring>();

        //set the buttonControl as child object under the selected gameobject
        buttonControl.transform.SetParent(Selection.activeTransform);

        buttonControl.transform.localScale = Vector3.one;
    }
}