﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonToggleUNET : MonoBehaviour
{
    // do not remove any enum here, instead add on, reason is all enumerator value will reset
    public enum TypeOfButton {HazardLight, SCDFSirenLightAndSound, SCDFSirenLight, SCDFSirenWailSound, SCDFSirenYelpSound, SCDFSirenHiLowSound,
        SCDFSirenPiercerSound, SCDFSirenWailRumbleSound, SCDFSirenYelpRumbleSound, SCDFSirenHiLowRumbleSound, SCDFSirenPiercerRumbleSound, SCDFRumble,
        SCDFSirenLightAndSoundControlPanel, SCDFSirenLightControlPanel, SCDFSirenSoundTurnOff
    }
    // drag in any buttontoggleunet that needs to be turned off when this button is activated
    public List<ButtonToggleUNET> AllSirenSoundTurnOffButtons = new List<ButtonToggleUNET>();
    // turn on for this if it is a toggle e.g. will switch between on or off
    public bool ToggleButton = false;
    // will send this bool across the network, set this as on start default state
    public bool TurnOnWhenPress = true;
    public TypeOfButton typeOfButton;
    // Start is called before the first frame update
    void Start()
    {
        // add onclick event listener on start
        this.GetComponent<UnityEngine.UI.Button>().onClick.AddListener(ButtonPressed);
    }

    // call this to make button to send a true value and initiate a button press * only for toggle button
    public void CallButtonOnSelected()
    {
        TurnOnWhenPress = true;

        ButtonPressed();
    }
    // call this to make button to send a false value and initiate a button press * only for toggle button
    public void CallButtonOnUnselected()
    {
        TurnOnWhenPress = false;

        ButtonPressed();
    }

    public void ButtonPressed()
    {
        // get the latest switch board values from UNetVarObj controller
        SwitchboardVarObj switchboardVarObj = UNetVarObjController.instance.GetUNetVarObj().switchboardVarObj;

        // if it's hazard light
        if (typeOfButton == TypeOfButton.HazardLight)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.HazardLightOn = true;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    switchboardVarObj.HazardLightOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    switchboardVarObj.HazardLightOn = true;
                }
                else
                {
                    switchboardVarObj.HazardLightOn = false;
                }
            }
        }
        // if it's siren light and sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenLightAndSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = true;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = true;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren light and sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenLightAndSoundControlPanel)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = true;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = true;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren light and sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenLightControlPanel)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                    switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // this will turn off all siren and make all other siren buttons turn off
        else if (typeOfButton == TypeOfButton.SCDFSirenSoundTurnOff)
        {
            // turn off
            switchboardVarObj.SirenLightOn = false;
            switchboardVarObj.SirenSoundWailOn = false;
            switchboardVarObj.SirenSoundYelpOn = false;
            switchboardVarObj.SirenSoundHiLowOn = false;
            switchboardVarObj.SirenSoundPiercerOn = false;
            switchboardVarObj.SirenSoundWailRumbleOn = false;
            switchboardVarObj.SirenSoundYelpRumbleOn = false;
            switchboardVarObj.SirenSoundHiLowRumbleOn = false;
            switchboardVarObj.SirenSoundPiercerRumbleOn = false;

            // check if need to turn off the other buttons
            if (AllSirenSoundTurnOffButtons.Count >= 1)
            {
                foreach (ButtonToggleUNET buttonToggleUNET in AllSirenSoundTurnOffButtons)
                {
                    // if this button is a toggle button...
                    if (buttonToggleUNET.ToggleButton == true)
                    {
                        // make this button turn on when press
                        buttonToggleUNET.TurnOnWhenPress = true;
                    }
                }
            }
        }

        // if it's siren light on
        else if (typeOfButton == TypeOfButton.SCDFSirenLight)
        {
            // turn on/off light
            if (switchboardVarObj.SirenLightOn == false)
            {
                switchboardVarObj.SirenLightOn = true;
            }
            else
            {
                switchboardVarObj.SirenLightOn = false;
            }

            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenLightOn = true;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenLightOn = false;
                }
            }
        }

        // if it's siren wail sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenWailSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenSoundWailOn = true;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    switchboardVarObj.SirenSoundWailOn = true;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren yelp sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenYelpSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = true;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = true;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren yelp sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenHiLowSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = true;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = true;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren Piercer sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenPiercerSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = true;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = true;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren Wail Rumble sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenWailRumbleSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = true;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = true;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren Yelp Rumble sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenYelpRumbleSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = true;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = true;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren HiLow Rumble sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenHiLowRumbleSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = true;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = true;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    //switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren Piercer Rumble sound on
        else if (typeOfButton == TypeOfButton.SCDFSirenPiercerRumbleSound)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = true;
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    // turn on
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = true;
                }
                else
                {
                    // turn off
                    //switchboardVarObj.SirenSoundWailOn = false;
                    //switchboardVarObj.SirenSoundYelpOn = false;
                    //switchboardVarObj.SirenSoundHiLowOn = false;
                    //switchboardVarObj.SirenSoundPiercerOn = false;
                    //switchboardVarObj.SirenSoundWailRumbleOn = false;
                    //switchboardVarObj.SirenSoundYelpRumbleOn = false;
                    //switchboardVarObj.SirenSoundHiLowRumbleOn = false;
                    switchboardVarObj.SirenSoundPiercerRumbleOn = false;
                }
            }
        }

        // if it's siren yelp sound on
        else if (typeOfButton == TypeOfButton.SCDFRumble)
        {
            // if it is a toggle button
            if (ToggleButton == true)
            {
                // if on press is true
                if (TurnOnWhenPress == true)
                {
                    //// turn on
                    // make next state
                    TurnOnWhenPress = false;
                }
                else
                {
                    //// turn off
                    TurnOnWhenPress = true;
                }
            }
            // if it is a fixed button
            else
            {
                if (TurnOnWhenPress == true)
                {
                    //// turn on
                }
                else
                {
                    //// turn off
                }
            }
        }

        // update message to sent to host
        UNetVarObjController.instance.UpdateUNetVarObj(switchboardVarObj);
    }
}
